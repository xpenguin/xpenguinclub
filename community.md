---
layout: page
title: Xpenguin - Community info
---
<div class="stallbop">
<img src="{{ site.baseurl}}/images/systemd.png" />
❤️ systemctl enable community ❤️
</div>

## Hexadoodle's Social thingabops

* [Discord](https://discord.gg/uRksPhD)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Twitter](https://twitter.com/hexdsl)
* [Instagram](https://www.instagram.com/hexdsl/)
* [IRC](#irc)

## The Official HexDSL IRC channel

<a name="irc"></a>
I have an IRC channel, the details you need to connect are below.

**SERVER:** Freenode

**CHANNEL:** #hexdsl

### How to use IRC?

{% include yt.html vidid="gQMUfCDgS-I" %}

### RULES

* Acceptable topics are Gaming, Linux, Technology. *Conversations flow and
change. Sometimes we will find ourselves discussing something off topic. That's
fine, but if you are the only one talking about something random and no one else is engaging you, then you may be asked to move back onto topic.*
* No spam/message flooding on the server.
* Respect other members.
* Anti-Linux posters are not welcome.
* Absolutely no NSFW or illegal content. *No porn, gore, pirated software. Nothing illegal is to be posted here. We judge the term "illegal" to mean anything not legal in nature inside the borders of the UK, USA or most Europeans nations.*

Rules are enforced at channel operators discretion.
