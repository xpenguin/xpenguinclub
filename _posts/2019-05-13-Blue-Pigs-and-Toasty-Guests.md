This week with added Arch Toasty! 
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)

* [Albion online](https://store.steampowered.com/app/761890/Albion_Online/)
* [Titan Quest: Atlantis](https://www.humblebundle.com/store/titan-quest-anniversary-edition?partner=hexdsl)
* [Swag and Sorcery](https://www.humblebundle.com/store/swag-and-sorcery?partner=hk)
* [Cheese bees](https://www.patreon.com/posts/small-project-26593300)
    * [twitter link](https://twitter.com/ValiantCheese/status/1127186122402983936 )

**IRC:** Freenode #hexdsl

### Toasty

**Games:** 

* [Hive Time](https://twitter.com/ValiantCheese/status/1127186122402983936)
* [Toast Time](https://store.steampowered.com/app/316660/Toast_Time/)
* [Portal](https://store.steampowered.com/app/400/Portal/)

## The Not News!

**[you can now support HexDSL on Liberapay if you would like to!](https://liberapay.com/hexdsl/)**

* [d9vk 0.10 released](https://github.com/Joshua-Ashton/d9vk/releases/tag/0.10)
* [d9vk now on Lutris!](https://lutris.net/bundles/d9vk)
* [EAC 'still provides linux support'](https://twitter.com/TeddyEAC/status/1125665801493798912)
* [Epic, EAC, the story so far!](https://www.pcinvasion.com/epic-games-linux-eac/)
* [Path of Exile, comments on linux support ](https://www.pathofexile.com/forum/view-thread/2500872)
* [Steam MV](https://github.com/tinfoil-hacks/steam-mv)
* [Fire fucked, a tale of mizillas screw ups ](https://hacks.mozilla.org/2019/05/technical-details-on-the-recent-firefox-add-on-outage/) *Addons are security holes!* 
* [Battle eye, coming to proton!](https://www.reddit.com/r/linux_gaming/comments/bn2t8v/battleye_is_working_with_valve_to_add_support_for/)
* [Steam-DOS](https://github.com/dreamer/steam-dos) play steam DOS games through NATIVE DOS-box. - early release, seems interesting. 
* [neon code devs are good humans!](https://fubenalvo.itch.io/neoncode)
 
## Contact us!

### ArchToasty 

* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Twitch](https://www.twitch.tv/archtoasty)
* [Mastodon](https://linuxrocks.online/@archtoasty)
* [Email](archtoasty@protonmail.com)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="HVKvaqSQa5Q" %}
