This week Hex is joined by uoou (see other names at bottom of document) where we talk a lot of nonsense as well as having a discussion about GTA V being the worst, Geneshift being pretty good, Mewnbase being relaxing, Deus Ex being amazing (he made me write this) and for some reason, the Sims 3 :|

<!--more--> 

### HexDSL

**Games:** 

* [Artifact](https://store.steampowered.com/app/583950/Artifact/)
* [Gene Shift](https://store.steampowered.com/app/308600/Geneshift/)
* [GTA V](https://store.steampowered.com/app/271590/Grand_Theft_Auto_V/)
* [MewnBase](https://store.steampowered.com/app/743130/MewnBase/)

### Friendo

* [Deus Ex: Mankind Divided](https://store.steampowered.com/app/337000)
* [Sims 3](https://store.steampowered.com/app/47890)
* [Overcooked 2](https://store.steampowered.com/app/728880)
* [Having fun with nspawn containers](https://wiki.archlinux.org/index.php/Systemd-nspawn)

## The Not News! 

* FORGET ALL OF THIS LAME NEWS! I HAVE A BOX OF ALHAMBRA! 
* [Linux.org got hacked](https://motherboard.vice.com/en_us/article/59vkq8/linuxorg-website-defacement-goatse)
* [No Epic store for Linux](https://www.unrealengine.com/en-US/blog/epic-2019-cross-platform-online-services-roadmap)
* [BeardedGiant games have a store now... great](https://www.gamingonlinux.com/articles/bearded-giant-games-open-their-own-store-with-a-linux-first-initiative.13165)
* [Discord Store coming to linux?](https://www.reddit.com/r/linux_gaming/comments/a68zzu/discord_developer_makes_a_verbal_commitment_to/)
* [Artifact updates coming](https://playartifact.com/dec13update)
* [Heroes of Newerth dropped Linux support](https://www.gamingonlinux.com/articles/heroes-of-newerth-drops-support-for-linux-and-mac.13168) that's HoN now Battle of/for Wesnoth, also not to be confused with Battle for Newerth. :\| (i liked battle for Newerth) 

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## Friendo ( uoou | Katniss Everdeen | Kattniss Everbean | Billy Hoyle | Drew | Uoniss | Piento | Drewberry Sundae | Andrew )

* [Ludic Linux](http://ludiclinux.com/)
* [Mastodon](https://linuxrocks.online/@uoou)
* [Twitch](https://www.twitch.tv/uouniss)

## On Youtube
{% include yt.html vidid="aWu67zwruBQ" %}
