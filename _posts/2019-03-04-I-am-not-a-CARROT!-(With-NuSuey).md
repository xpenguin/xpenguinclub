fuck fuck fuck, fuck foooook fak! 

<!--more--> 

### HexDSL

**Games:** 

* [Tabletop sim (carcassonne)](https://store.steampowered.com/app/286160/Tabletop_Simulator/)
* [Retroarch](http://www.retroarch.com/)
* [Fall of Dungeon Guardians](https://store.steampowered.com/app/409450/The_Fall_of_the_Dungeon_Guardians__Enhanced_Edition/)
* [Mavis Beacon](https://www.ebay.co.uk/itm/Mavis-Beacon-Teaches-Typing-Platinum-Version-20-by-Beacon-Mavis-DVD-ROM-Book/113492042677?epid=88607169&hash=item1a6ca723b5:g:3J8AAOSwznxcBnpX:rk:2:pf:0)

* Bonus - Hex's stupid Keyboard update.

### NuSuey

**Games:** 

* [Rocksmith 2014](https://store.steampowered.com/app/221680/Rocksmith_2014_Edition__Remastered/)
* [Galaxy Squad](https://store.steampowered.com/app/921710/)
* [Grounds of Glory](https://store.steampowered.com/app/869120/)
* [Destiny or Fate](https://store.steampowered.com/app/919220/Destiny_or_Fate/)

## The Not News! 

* [DXVK 1.0](https://www.gamingonlinux.com/articles/dxvk-the-project-for-d3d11-and-d3d10-over-vulkan-hits-the-big-10.13640)
* [Behold D9VK](https://www.phoronix.com/scan.php?page=news_item&px=D9VK-D3D9-Vulkan-From-DXVK)
* [OBS 23.0](https://github.com/obsproject/obs-studio/releases/tag/23.0.0)
* [Armello 2.0](https://steamcommunity.com/games/290340/announcements/detail/1757997476049796958)
* [Dead Cells DLC alpha](https://steamcommunity.com/games/588650/announcements/detail/3534666999993024072)
* [GOG lays off a dozen employees](https://www.rockpapershotgun.com/2019/02/26/gog-layoffs/)
    
## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

### NuSuey 

* [Patreon](https://www.patreon.com/tuxdb)
* [YouTube](https://www.youtube.com/user/NuSuey)
* [Twitch](https://www.twitch.tv/nusuey) 
* [tuxDB](https://tuxdb.com)

## On Youtube
{% include yt.html vidid="i3RIL2UOEYA" %}
