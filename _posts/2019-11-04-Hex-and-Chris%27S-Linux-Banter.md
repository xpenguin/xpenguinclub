Hex and Chis do Linux banter! Topics include MineTest, Hitman™, OpenArena LIVE, and a LOT about RSS readers. 
<!--more--> 

[A onetab link](https://www.one-tab.com/page/miQK65QARD-g7hyP9hNhdg)

### HexDSL

* [UnderMine](https://store.steampowered.com/app/656350/UnderMine/)
* [CogMind](https://store.steampowered.com/app/722730/Cogmind/)
* [Space Grunts](https://store.steampowered.com/app/371430/Space_Grunts/)
* [Odd realm](https://store.steampowered.com/app/688060/Odd_Realm/)

## Chris Were

* [Minetest - Hexico](https://minetest.net) [footage](https://youtu.be/_GKSndz493s?t=2461)
* [Opena Arena](https://openarena.live/) [footage](https://youtu.be/coRdXHXsBq0?t=132)
* [Paint the Town Red](https://store.steampowered.com/app/337320/Paint_the_Town_Red/)
* [Hitman™ 2 - Halloween pack](https://store.steampowered.com/app/863550/HITMAN_2/)

### Not The News

* RSS DEEP DIVE! (Topic)
    * [The Old Reader](https://theoldreader.com)
    * [Inoreader](https://www.inoreader.com)
    * [Feedly](https://www.feedly.com)
    * [New Boat](https://newsboat.org/)
* [New Steam Library is now released to everyone, and they dont like it](https://www.gamingonlinux.com/articles/valve-rolls-out-the-new-steam-library-and-remote-play-together-for-everyone.15318) *I had MANY a youtube comment!* 
* [Remote Play Together 'fixed'](https://steamcommunity.com/groups/SteamClientBeta/announcements/detail/1603772016002337493) *Janky*
* [Brian Kernaghans new book.](https://hackaday.com/2019/10/29/unix-tell-all-book-from-kernighan-hits-the-shelves/) *NOT KINDLE COMPATIBLE!!!*

## Contact us:

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [My Website](http://hexdsl.co.uk)
* [.XPenguin Website](http://xpenguin.club)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

### Chris Were

* [Website](https://chriswere.uk/)

## On Youtube
{% include yt.html vidid="CzV6VAUSTTk" %}# 
