This week ArchToasty joins Hex to talk about Dre amHack! 
<!--more--> 

### HexDSL  

**Games:** 
* [The Elder Scrolls IV: Oblivion](https://store.steampowered.com/app/22330/The_Elder_Scrolls_IV_Oblivion_Game_of_the_Year_Edition/)
* [OpenMW](https://openmw.org/en/)

I also UNINSTALLED DISCORD this week.. (switched to browser based discord!)

### ArchToasty! 

**Games:** 
* [Super Radish Witch](https://danbolt.itch.io/superradishwitch) Free :)
* [Overwatch](https://playoverwatch.com) on sale?
* [Rocket League](https://store.steampowered.com/app/252950/Rocket_League/) on sale? (I played child appropriate games! mostly)

## The Not News! 

* [Overwatch bans?](https://www.reddit.com/r/linux_gaming/comments/9yl56p/new_overwatch_ban_caused_by_an_autoclicker_not/)
* [your cpu is shit](https://www.phoronix.com/scan.php?page=news_item&px=Linux-Stable-Dropping-STIBP)
* [DreamHack Video for easy clicking](https://www.youtube.com/watch?v=8wqPBG2fSfs)

## Contact us!

### ArchToasty

* [Twitter](https://twitter.com/archtoasty)
* [Twitch](https://www.twitch.tv/archtoasty)
* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Mastodon](https://linuxrocks.online/@archtoasty)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube
{% include pt.html vidid="20e1b1bf-c9d9-404e-ae54-361c0c796973" %}
