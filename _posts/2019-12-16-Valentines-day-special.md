valentines day special. (Becuse that title will confuse the shit out of the occasional person for MONTHS!) 
<!--more--> 

[A onetab link](https://www.one-tab.com/page/j3sI_henRyG2fklBURxgFg)

### HexDSL

* [Halo Reach](https://store.steampowered.com/app/1064220/Halo_Reach/)
* [Titan Quest *on ANDROID!*](https://play.google.com/store/apps/details?id=com.dotemu.titanquest&hl=en_GB)

### ArchToasty

* [Odd Realm](https://store.steampowered.com/app/688060/Odd_Realm/)

### Not The News

* LOOK! you can stream on DISCORD now!!!
* [DXVK = Dead Xterminated Very Killed](https://github.com/doitsujin/dxvk/pull/1264)
* [Proton Update 4.11-10](https://github.com/ValveSoftware/Proton/releases/tag/proton-4.11-10) *the MC:C Update!*
* [Google BANNING some Linux web Browsers (not secure!)](https://www.bleepingcomputer.com/news/google/google-now-bans-some-linux-web-browsers-from-their-services/)
* [Toasty Getting a PinePhone](https://store.pine64.org/?product=pinephone-braveheart-limited-edition-linux-smartphone-for-early-adaptor)

## Contact us:

### TOASTY

* You know where to find him! 

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [My Website](http://hexdsl.co.uk)
* [.XPenguin Website](http://xpenguin.club)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

## On Youtube

{% include yt.html vidid="KVaBTcPn004" %}
