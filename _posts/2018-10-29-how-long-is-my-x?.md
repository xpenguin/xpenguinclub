This week Hex takes on the internet alone to talk with the twitch chat, fanboi over Nier:automata (again) and ask how long the xpenguin show should be. Then in the news section there's talk about developers who revoke keys for bad reviews, gamespots Linux section and more... 
<!--more--> 

## Host Information:

### HexDSL 

**Games:** 

* [NieR: Automata](https://store.steampowered.com/app/524220/NieRAutomata/) *SteamPlay*
* [Dicey Dungeons](http://diceydungeons.com/) *Itch Early Access*

## Not news!

* FEEDBACK WANTED: Do we need a forum/sub-reddit (..and which one?)
* [FEEDBACK WANTED: is the show too long? (multiple people have commented on this)](http://www.strawpoll.me/16723841)
* [Gamespot has a Linux area now!](https://www.gamespot.com/unixlinux)
* [Developers removes game access for a negative review](https://www.gamingonlinux.com/articles/game-developer-revokes-a-users-steam-key-after-negative-review.12787) *wanker* 
* [Rings of Saturn has a steam demo and dev seems nice!](https://www.reddit.com/r/linux_gaming/comments/9r7rf6/i_promised_firstclass_day_one_linux_support_for/)
* [Total War: Warhammer II details](https://www.gamingonlinux.com/articles/linux-port-of-total-war-warhammer-ii-due-as-soon-as-possible-after-curse-of-the-vampire-coast.12797) 

## Contact me!yy

### HexDSL

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube
{% include pt.html vidid="b1f74cfd-f1a2-4d61-8644-7cf12986adbd" %}
