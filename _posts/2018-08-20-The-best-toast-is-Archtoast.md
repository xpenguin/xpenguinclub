This week I am joined by streamer/community member ArchToasty for some hardcore Ballistic overkill talk and some DXVK discussion. I talk about Lazy Galaxy and Graveyard keeper. In the news section we discuss steams seemingly bold move towards platform agnostic gaming, Banner sagas abandonment and some awesome Blender benchmarking. 
<!--more--> 

## Host Information:

### HexDSL 

**Games:** 

* [Lazy Galaxy: Rebel Story](https://store.steampowered.com/app/533050/Lazy_Galaxy_Rebel_Story/)
* [Graveyard Keeper](https://www.graveyardkeeper.com/)

**This week:** 

* [Xpenguin website](http://xpenguin.club) is now free from Microsoft as Drew (uoou) migrated the whole site over to gitlab last night! - also My Dot files are gitlab now! 
* Left Dropbox!- Now using [NextCloud](https://duckduckgo.com/l/?kh=-1&uddg=https%3A%2F%2Fnextcloud.com%2F) and [Syncthing](https://duckduckgo.com/l/?kh=-1&uddg=https%3A%2F%2Fsyncthing.net%2F) to test syncthing i used it for moving 480GB between 2 pcs yesterday! worked fine, android app is shite though. 
* All My videos will be on peertube from now on. 

**Current Distro:** Antergos (i3wm)

### ArchToasty

**Games:** 

* [Ballistic Overkill](https://store.steampowered.com/app/296300/Ballistic_Overkill/)
* [Warframe](https://lutris.net/games/warframe/)
* [Overwatch](https://lutris.net/games/overwatch/) 
    
**This week:** 

* [Topic - DXVK jumping to version 0.70](https://github.com/doitsujin/dxvk/releases)
**Current Distro:** Arch (KDE)

## The Not News! 

* [HAMISH IS RETURNING IT STREAMING!- Starting Tuesday!](https://hamish.thepolarbear.co.uk/)
* [Obsidian, Microsoft.... *nooo!*](https://gamestechica.com/2018/08/08/obsidian-entertainment-letter-of-intent-for-an-acquisition-by-microsoft-leaked/)
* [Banner Saga no longer officially supported](https://www.gamingonlinux.com/articles/the-original-the-banner-saga-is-no-longer-officially-supported-on-linux.12344) looks like RUST started a trend!
* [AWAY TEAM UPDATE!](https://underflowstudios.com/the-away-team-lost-exodus-redesigning-the-argo/)
* [Steam-Play?](https://arstechnica.com/gaming/2018/08/valve-seems-to-be-working-on-tools-to-get-windows-games-running-on-linux/)
* [Blender Benchmark](https://www.blender.org/news/introducing-blender-benchmark/)
    * [open benchmark](https://opendata.blender.org/) Maybe not the most gaming related, but it shows that blender is setting a great example for how to collect data and then make it available to everyone for general betterment.
* [hexs crusade against plugins!](https://www.reddit.com/r/linux/comments/982jw9/mozilla_removes_23_firefox_addons_that_snooped_on/) mozilla removed 23 plugins that snooped on users! 

## Contact me!

### TOASTY *(The Toast with the Most)*

* [Twitch](https://www.twitch.tv/archtoasty)
* [twitter](https://twitter.com/archtoasty)
* [Mastodon](@archtoasty@linuxrocks.online)
* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Email](archtoasty@pm.me)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/video-channels/63feea73-94f9-4fb6-93f2-8db11a9eec63/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://mastodon.rocks/@HexDSL)
* [Twitter](https://twitter.com/HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube
{% include pt.html vidid="3acbd1af-ba9a-4b5d-ba95-65213f2058df" %} 
