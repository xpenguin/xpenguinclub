This week Hex had a power cut. so did not play much, Talk about much or Read Much... But chat was there so it was fine!
<!--more--> 

### HexDSL

**Games:** 

* [Pathway](https://store.steampowered.com/app/546430/Pathway/)
* [Forager](https://store.steampowered.com/app/751780/Forager/) *is it a clicker? I don't know!* 
* [Driftland](https://store.steampowered.com/app/718650/ ) *literally played like 10 mins of it* 

## The Not News!
* [Epic store games working on Lutris](https://twitter.com/LutrisGaming/status/1118552969816018948?s=19) and Tim suggested Strider apply for a grant.
* [No Mans Sky Vulkan update!](https://www.nomanssky.com/2019/04/vulkan-update/) sure its buggy now, but im pretty sure it will be great within a few updates. (beta code is 3xperimental)
    * [They answered an email about it running shite](https://i.imgur.com/sCGRnzm.jpg)
* [Caves of Qud getting price increase](https://www.gamingonlinux.com/articles/caves-of-qud-the-crazy-deep-roguelike-is-having-a-price-increase-this-week-so-act-fast.13953)
* [Steam Update](https://store.steampowered.com/news/50095/) *Fixed more instances of 0-byte downloads*

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="vv20ufSGavA" %}
