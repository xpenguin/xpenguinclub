This week Hex is joined by the infamous Samsai, who is not the Steam-play superfan that Hex is! we discuss FATE, Torchlight, Dungeon Crawl and a bunch more games as well as Steam-play ranking websites and the failings of Wine. Theres also that Doom 2 milestone that entertained Hex. 
<!--more--> 

## Host Information:

### HexDSL 

**Games:** 

* [FATE](https://store.steampowered.com/app/246840/FATE/) - *SteamPlay*
* [Torchlight](https://store.steampowered.com/app/41500/Torchlight/) - *SteamPlay*
* [Lazy Galaxy: Rebel Story](https://store.steampowered.com/app/533050/Lazy_Galaxy_Rebel_Story/)
* [Borderlands: The Pre-Sequel](https://store.steampowered.com/app/261640/Borderlands_The_PreSequel/) - *streamed*

**This week:** 

### Samsai 

**Games:** 

* [Dungeon Crawl: Stone Soup](https://crawl.develz.org/) - *tons of it*
* [Tower of Time](https://www.gog.com/game/tower_of_time)
* [Arx Fatalis](https://www.gog.com/game/arx_fatalis) - *Arx Libertatis*

**This week:** 

## The Not News! 
* [Why is it called PROTON?](https://github.com/ValveSoftware/Proton/issues/642#issuecomment-416262220)
* [Proton checker website](https://proton.city/)
    * [and the other one](https://spcr.netlify.com/) *this is the 'main' one*
* [Forbes say over 1000 games!](https://www.forbes.com/sites/jasonevangelho/2018/08/27/steam-for-linux-adds-1000-perfectly-playable-windows-games-in-under-a-week/#1b2d874a55ae)
* [Valve have rolled out Steam Play into the stable Linux Steam Client, along with touch controls for Steam Link](https://www.gamingonlinux.com/articles/valve-have-rolled-out-steam-play-into-the-stable-linux-steam-client-along-with-touch-controls-for-steam-link.12448)
* [X-COM 2 expansion coming?](https://www.gamingonlinux.com/articles/xcom-2-to-possibly-get-another-expansion-with-tle.12472)
* [Twitch working to remove 3rd party video-players?](https://github.com/vinszent/gnome-twitch/issues/367)
* [DOOM 2 finally 100% done. After 24 years](https://www.polygon.com/2018/8/31/17807464/doom-2-last-secret-discovered-map-15-level-15) *not linux but made me happy* 

## Contact us! 

### Samsai!

* [Mastodon](https://mastodon.social/@Samsai/)
* [Twitch](https://www.twitch.tv/sirsamsai)
* [Twitter](https://twitter.com/thesamsai)
* [YouTube](https://www.youtube.com/channel/UCabcRjtkVtf1g41wN71RHOg)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Twitter](https://twitter.com/HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube
{% include pt.html vidid="a7322d64-1c82-4a3c-a344-fd5f5483bace" %}
