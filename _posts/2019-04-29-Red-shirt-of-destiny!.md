After a whole week with electric flowing like, err, electric! Hex has things to say! things to talk about and things to bash! (STK1.0) - Coda talks D and D, Splinter Cell and star craft. all in all, it was a pretty great show... but I would say that!
<!--more--> 

### HexDSL

**Games:** 
 
* [Chrono Trigger](https://store.steampowered.com/app/613830/CHRONO_TRIGGER/)
* [Thea 2: The shattering](https://store.steampowered.com/app/606230/Thea_2_The_Shattering/)
* [No Mans Sky](https://store.steampowered.com/app/275850/No_Mans_Sky/)
* [Final Fantasy 14](https://lutris.net/games/final-fantasy-xiv-a-realm-reborn/)
* [I uninstalled Forager](http://ludiclinux.com/Forager/) BECAUSE ADDICTION 
* I also played SMITE in the Switch. it was great!
* Humble server is real. (mumble.xpenguin.club) - Discord replacements are quite a way off.

### JayCoda

**Games:** 

* [Capcom Sale](https://store.steampowered.com/sale/capcom)
* [Dungeons & Dragons Beat em Up](https://store.steampowered.com/app/229480/Dungeons__Dragons_Chronicles_of_Mystara/?curator_clanid=33273264)
* [Splinter Cell](https://store.steampowered.com/app/13560/Tom_Clancys_Splinter_Cell/)
* [StarCraft Remastered](https://lutris.net/games/starcraft-remastered/)
* I also played Pokemon Ultra Sun on my Nintendo 3DS, recently got all three original starters. 

## The Not News!

* [That Stallman interview from last week!](https://www.invidio.us/watch?v=9c3sv30w158)
* [Super Tux Kart 1.0 is out.... rejoice?](http://blog.supertuxkart.net/2019/04/supertuxkart-10-release.html)
* [A Great video showing off Raytracing in OpenGL](https://www.youtube.com/watch?v=5jD0mELZPD8) *thanks Silmeth on IRC
* [Systemd Service to swap CTRL keys for Overwatch](https://www.reddit.com/r/linux_gaming/comments/bhedxh/i_wrote_a_simple_systemd_service_to_fix_control/)
* [Boiling Steam - that one graph](https://boilingsteam.com/proton-one-graph-to-sum-it-all/) 
* [Hero-U is now on Itch](https://transolargames.itch.io/hero-u-rogue-to-redemption)
* [Nvidia actually working on stuff!](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-EXT-Vendor-Select)
* [Total War: WARHAMMER II - The Prophet & The Warlock Released For Linux](http://www.feralinteractive.com/en/news/972/)
* [VMware really want to be relevant again](https://www.phoronix.com/scan.php?page=news_item&px=Emulated-Coherent-Video-RAM)


## Contact us! 

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

### Coda *(JayCoda)*

* [Twitter](https://twitter.com/jay_coda)
* [Twitch](http://twitch.tv/jay_coda)
* [Website](http://emberphoenix.net)
* [Email](mailto:coda.the.otaku@gmail.com)

## On Youtube
{% include yt.html vidid="qtfw5i-IXSg" %}
