QnA, QnA, QnA! this episode is part Q&A and part normal show. Its an awesome hybrid. Also I let Colin Baker earlier and that was awesome!!! 
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)

* [Ren'py](https://www.renpy.org/)
* [Aseprite](https://store.steampowered.com/app/431730/Aseprite/)
    * [GRAFX2](http://grafx2.chez.com)
* [Space Mercs](https://store.steampowered.com/app/1088600/Space_Mercs/)

### Not News... 

* [Is porting games to linux a waste of time - FORBES](https://www.forbes.com/sites/jasonevangelho/2019/08/07/porting-games-to-linux-is-a-waste-of-time-this-game-developer-says-youre-doing-it-wrong/#18da8b9c2c16)
* [Nvidi-HA!](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-Open-GPU-Docs)
* [Shut up Gary](https://rust.facepunch.com/blog/linux-plans)

### Q & A: 

* **Q** Do you only use free software (cant remember who asked this) 
* **Q** Will you Play X4 (6 people, every week... 1 year!) 
* **Q** Are you wearing trousers (Larsk IRC)
* **Q** "what OS security related advice would you give to a new Linux user? I have been hearing about dependencies being vulnerable and any advice on how to check if s/w i am downloading is safe or not, on windows you just run it by the AV program." (Sambiohazard)
* **Q** "Apart from on pancakes, what other nice recipes can I use maple syrup in?" (Hamsish)
* **Q** "Dear Hex, I fear my boyfriend may be cheating on me. Ever since a few months ago when he bought a new project car, he's seemed distant and preoccupied. He never wants to spend time with me and always seems to be out at "car meets" or "track days" with people I've never met. I've tried asking him where these things are but he dismisses me because he's afraid of our Alexa "narcing" on him and "his crew" to the cops, or other seemingly ridiculous justifications. - Please help me. I'm sure he's still a good man inside, I just want to know if I should try to win him back!" (Communistopher) 
    * **A** “Yes it is possible to construct a cannon that launches them at lethal velocity but the barrel will need to be somewhere on the order of 15-20 meters long” 
* **Q** Is it true that if you pour Coca Cola on a block of Swiss cheese, within an hour a cuckoo pops out of one of the holes, perhaps as many as 12 times, and goes, "cuckoo!"? (Jumble sailor)

## Contact us!



### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)
  
## On Youtube
{% include yt.html vidid="sY7FXieJjrM" %}
