## Chris's section

### Games

* [Assassin's Creed Odyssesy](https://stadia.google.com/store/details/8b7e7f7036e5483eaa8745d46248536crcp1/sku/6760aad6e75b4edc9686c48e8dd38936)
* [Hunt for the Shadow Rider](https://bluestreak0.itch.io/hunt-for-the-shadow-rider)
* [Portal](https://nezvers.itch.io/portal)
* [Destiny 2](https://stadia.google.com/store/details/20e792017ab34ad89b70dc17a5c72d68rcp1/sku/bd70626ec3834dedbc6dda5b956f7648)

### Social media links

* [Twitch](https://twitch.tv/ChrisWere)
* [Mastodon](https://linuxrocks.online/@ChrisWere)
* [Website](https://ChrisWere.uk)


## Ky's section

### Games
* [Praey for the Gods](https://store.steampowered.com/app/494430/Praey_for_the_Gods/)
* [Earth Defense Force 4.1](https://store.steampowered.com/app/410320/EARTH_DEFENSE_FORCE_41_The_Shadow_of_New_Despair/)
* [Code Vein](https://store.steampowered.com/app/678960/CODE_VEIN/)

### Social media links

* [Ky on YouTube](https://youtube.com/kylinuxcast)
* [Mastodon](https://linuxrocks.online)

## News

* [Ballistic Overkill Loses Support](https://www.gamingonlinux.com/articles/aquiris-game-studio-ending-support-for-their-online-fps-ballistic-overkill.15553)
* [Disney’s Tron: Evolution No Longer Playable](https://happygamer.com/disneys-tron-evolution-no-longer-playable-by-anyone-due-to-securoms-drm-45265/)
* [Newsblue](https://newsblur.com)
