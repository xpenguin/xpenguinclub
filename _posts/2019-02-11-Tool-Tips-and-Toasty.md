This week ArchToasty and I talk about Steam Awards, Broken games, Tool Tips, World of Warcraft making lives better and some other stuff. 
<!--more--> 

### HexDSL

**Games:** 

I just uninstalled a bunch of stuff and broke stuff! 

* [At The Gates](https://store.steampowered.com/app/241000/Jon_Shafers_At_the_Gates/)
* [Spinnortality](https://store.steampowered.com/app/822990/Spinnortality__cyberpunk_management_sim/) *Wasn't Cyberpunk*
* [Paladins](https://store.steampowered.com/app/444090/Paladins/) *Proton*
* [The Sims 3](https://store.steampowered.com/app/47890/The_Sims_3/) *Proton*

Groff stuff

### ArchToasty

**Games:** 

* [Fallout4](https://store.steampowered.com/app/377160/Fallout_4/) *Lutris*
* [Wargroove](https://store.steampowered.com/app/607050/Wargroove/) *Proton*
* [Ballistic Overkill](https://store.steampowered.com/app/296300/Ballistic_Overkill/) *doesn't work any way you play it, just don't buy this game*

## The Not News! 

* [This wonderful story from the BBC about a WoW player](https://www.bbc.co.uk/news/disability-47064773) *its not new or even Linux related but I enjoyed reading it* 
* [Apex Legends, broken on Linux](https://www.reddit.com/r/linux_gaming/comments/ao01l8/despite_working_perfectly_at_launchapex_legends/)
* [GreenWithEnvy - Nvidia Overclocking tool](https://gitlab.com/leinardi/gwe)
* [EAC doesnt work in wine.... hummm](https://www.reddit.com/r/linux_gaming/comments/aof2f2/with_the_recent_problems_regarding_the_eac_did/)  Also, Paladins stopped working last night!
* [Steam Awards](https://store.steampowered.com/steamawards/) 😐😐😐😐

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

### ArchToasty 

* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Twitch](https://www.twitch.tv/archtoasty)
* [Mastodon](https://linuxrocks.online/@archtoasty)
* [Email](archtoasty@protonmail.com)

## On Youtube
{% include yt.html vidid="cNZXmtiWqF0" %}
