Toasty here, he didnt play anything so Hex talked, a lot! Hex Talked Civ5, Cat Quest 2, Noita, and more!
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)
* [MINDUSTRY!](https://store.steampowered.com/app/1127400/Mindustry/) *Thanks for tip Popey* 
* [Cat Quest 2](https://store.steampowered.com/app/914710/Cat_Quest_II/) *REFUNDED!*
* [SEGA CLASSICS](https://store.steampowered.com/app/34270/SEGA_Mega_Drive_and_Genesis_Classics/) *WHAT A LOAD OF OF SHIT!*
* [CIV 5](https://store.steampowered.com/app/8930/Sid_Meiers_Civilization_V/)
* [Noita](https://store.steampowered.com/app/881100/Noita/)
* [Jalopy](https://store.steampowered.com/app/446020) *Could not get out of car*
* Also, [My website is now powered by BLOP blog system.](https://gitlab.com/uoou/blop) Thanks Drew!

### ArchToasty

HE PLAYED HEXS GAMES!

### Not News... 

* [Twitch pays OBS!](https://obsproject.com/blog/twitch-becomes-premiere-sponsor-of-the-obs-project) *not linux but important to me!*
* [HEADLESS RENDERING IS COMING!!](https://www.gamingonlinux.com/articles/15082)
* [Space Engineers dev did some proton work to help MONO work](https://github.com/ValveSoftware/Proton/issues/1792)
* [Steam remote updates](https://www.gamingonlinux.com/articles/valve-are-expanding-steam-remote-play-with-defaults-for-popular-games-and-a-new-api.15105)

## Contact us!

### ArchToasty

* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Twitch](http://twitch.tv/archtoasty)
* [Mastodon](@archtoasty@linuxrocks.online)

### HexDSL *(Sexy Hexy)*

* [MY WEBSITE](http://hexdsl.co.uk)
* [MY Art Website (Yes, Really)](http://pixelfridge.club)
* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)
  
## On Youtube
{% include yt.html vidid="91QhN6CL6UY" %}
