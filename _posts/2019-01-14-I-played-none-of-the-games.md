An XXX penguin
<!--more--> 

### HexDSL

**Games:** 

* This week I have mostly played LINUX! the game! (as you can see from this weeks content)
    * QTILE!
    * Search Engines
    * I3 Update!
    * Password Manager
* [Katamari](https://store.steampowered.com/app/848350/Katamari_Damacy_REROLL/) *thanks ekianjo*

## The Not News! 

* [steam updated!](https://store.steampowered.com/news/47430/)
* [Cultist Simulator has update coming](https://www.gamingonlinux.com/articles/cultist-simulator-to-get-a-free-update-this-month-with-a-new-game-mode-and-challenges.13309)
* [Paradox purchased Prison Architect](https://www.gamingonlinux.com/articles/paradox-interactive-have-purchased-the-rights-to-prison-architect-from-introversion.13308)
* [Boiling steam made a word cloud!](https://boilingsteam.com/a-word-cloud-to-describe-proton/)
* [Smith and Winston developer: "How I support Windows, Mac and Linux"](https://www.executionunit.com/blog/2019/01/02/how-i-support-windows-mac-and-linux/)
    * [Smith and Winston on steam](https://store.steampowered.com/app/942250/Smith_and_Winston/) 
* [Super Tux Kan't has Mutiplayer now](https://www.omgubuntu.co.uk/2019/01/supertuxkart-online-beta-testing)

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="itH2330lb5I" %}
