this week I have a full on love-in with Ys, talk about open souce gaming, again and steams new readesign. This and much more in this weeks action packed show
<!--more--> 

### HexDSL

* [Age of Wonders: Planet Fall](https://www.humblebundle.com/store/age-of-wonders-planetfall?partner=hexdsl)
* [Ys 1 (Chronicles)](https://store.steampowered.com/app/223810/Ys_I__II_Chronicles/)

* [My open source game thoughts](https://youtu.be/Ax9xRCMNDso)

### Not News... 

* [NEW STEAM NEW!](https://steamcommunity.com/games/593110/announcements/detail/1608269907266250853)
* [SM64 port!](https://www.youtube.com/watch?v=8882hqPS3WQ&feature=youtu.be) *Nintendo will not like this, no, not at all*
* [Code Weavers hiring for Proton development](https://www.gamingonlinux.com/articles/codeweavers-still-looking-for-more-developers-to-work-on-steam-play-proton.14947)
* Firefox updating, no longer loading chrome.css - toolkit.legacyUserProfileCustomizations.stylesheets preference  = true for css loading.-

## Contact Me

### HexDSL *(Sexy Hexy)*

* [MY WEBSITE](http://hexdsl.co.uk)
* [MY Art Website (Yes, Really)](http://pixelfridge.club)
* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

## On Youtube
{% include yt.html vidid="SGK2wAuwqJc" %}

