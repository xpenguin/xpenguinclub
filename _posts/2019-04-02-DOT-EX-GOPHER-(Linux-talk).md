This week we talk WAY too much about Gopher and BBS stuff (ah, great!) as well as Risk of Rain 2, Tropico 6 and Gamehub. When it comes to news, well... things i like to talk about, there was Factorio prices, DiRT4 and Vampire games... 
<!--more--> 

### HexDSL

**Games:** 

* [Factorio](https://store.steampowered.com/app/427520/Factorio/)
    * FACTORIO SERVER! 
* [Tropico 6](https://store.steampowered.com/app/492720/Tropico_6/)
* [Risk of rain 2](https://store.steampowered.com/app/632360/Risk_of_Rain_2/)

* [Gopher](https://aur.archlinux.org/packages/cgo-git/)
    *    gopher://gopher.xpenguin.club

* I tried [GameHub.](https://github.com/tkashkin/GameHub) My Steam Library killed it! 

## The Not News!

* [Factoio price change](https://www.factorio.com/blog/post/016-price-change)
* [DiRT 4 has been ported](https://store.steampowered.com/app/421020/DiRT_4/)
* [Vampire: Masqurade Bloodline 2, maybe coming to Linux](https://www.gamingonlinux.com/articles/looks-like-vampire-the-masquerade-bloodlines-2-may-be-coming-to-linux.13847)
* [An Artifact post thingy](https://steamcommunity.com/games/583950/announcements/detail/1819924505115920089)
* [That Valve Headset](https://store.steampowered.com/sale/valve_index/)


## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="c1kFk8YmVxM" %}
