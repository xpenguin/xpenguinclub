This week Hex got to talk with Glorious Eggroll about Wine, DXVK, Proton, FNA-Faudio and a bunch of other things. Eggroll is smart and Hex is a simple gamer, I think he learned a lot about the way that Wine works and everyone had a nice time. Please enjoy this extra long .XPenguin...
<!--more-->

## Host Information:

### HexDSL

**Games:**

* [Rimworld](https://store.steampowered.com/app/294100/RimWorld/)
* [Polygod](https://store.steampowered.com/app/531530/Polygod/)
* [Diablo 3](http://eu.blizzard.com/en-gb/games/d3/) *Wine/DXVK*
* [Overwatch](https://www.humblebundle.com/monthly?partner=hexdsl) *its in a bloody humble bundle!!!*

**This week:**

Now have DOTS thing! [dots.xpenguin.club](http://dots.xpenguin.club)

### GloriousEggroll

**Games:**
* [Warframe on Steam Play](https://store.steampowered.com/app/230410/Warframe/) *Wine/DXVK*
    * [Eggroll's Warframe Steam Play Launcher](https://gitlab.com/GloriousEggroll/warframe-linux/tree/steamplay-proton)
* [Path of Exile](https://www.pathofexile.com/game) *Wine/DXVK*
    * [PoE Patch needed for wine](https://gist.github.com/DataBeaver/5e1f1a256c7f1abb339831fc2b72a5db)
    * [PoE anti-stutter Patch needed for DXVK](https://github.com/jomihaka/dxvk-poe-hack)

**This week:**


## The Not News!

* [Life is Strange, Before the Storm coming to linux SOON](https://store.feralinteractive.com/en/mac-linux-games/lisbeforethestormdeluxe/)
* [We Happy Few still coming to Linux](https://www.gamingonlinux.com/articles/an-update-on-the-linux-version-of-we-happy-few.12504)
* [Athenaeum, itsa thing](https://gitlab.com/librebob/athenaeum)
* [Vulkan extension to add stream output support](https://github.com/KhronosGroup/Vulkan-Ecosystem/issues/26#issuecomment-418530373)
    * Details: The purpose of the stream-output stage is to continuously output (or stream) vertex data from the geometry-shader stage (or the vertex-shader stage if the geometry-shader stage is inactive) to one or more buffers in memory. The problem is there is no current way to translate this behavior from Direct X 11 to Vulkan. [Diagram of stages](https://docs.microsoft.com/en-us/windows/desktop/direct3d11/d3d10-graphics-programming-guide-output-stream-stage)
* [ProtonTricks](https://github.com/Sirmentio/protontricks)
* [Wine's major Xaudio problem and what is FAudio?](https://github.com/FNA-XNA/FAudio)

* **Eggroll's mastodon gripes:**
    * As a long time bird site user who is new to Mastodon, I love the idea behind the platform, and I want it to succeed. That being said, I have a few problems I'd like to address:
    * Let me quote a tweet with my own response. Currently it's only restricted to retweeting
    * Can we not use a default layout that looks like tweet deck? it's a jarbled mess. give me just my home feed with tabs or a navbar for the others. Also make the home feed take up a larger portion of the page. Reading a tiny column is awful.
    * Let me see my own info somewhere besides another column. i dont need a whole column,just a small area that shows it. This way I can decide if i want to change it while I'm browsing.
    * WHO. TO. FOLLOW. Let me see suggested people to follow based on people I'm already following. This will help the platform grow tremendously
    * TRENDING HASHTAGS. Give me an area that shows trending hashtags. This is how people communicate on a hot topic. They see a topic brewing, they want to join in.

## Contact us!

## GloriousEggroll

* [Blog](http://gloriouseggroll.tv)
* [GitLab](http://gitlab.com/gloriouseggroll)
* [YouTube](http://youtube.com/user/gloriouseggrolltv)
* [Twitch](http://twitch.tv/gloriouseggroll)
* [Mastodon](https://linuxrocks.online/@GloriousEggroll)
* [Twitter](https://twitter.com/GloriousEggroll)
* [Email](mailto:gloriouseggroll@protonmail.com)
* [Patreon](https://www.patreon.com/gloriouseggroll)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Twitter](https://twitter.com/HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube
{% include pt.html vidid="51bc33a1-7a42-44c6-a29c-4afe37d8a9ac" %}
