Hex and Arch Toasty talk about TOASTERS.... and the Wu-Tang Clan. And I *think* we talked about video games at one point... 
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)

* [Ren'py](https://www.renpy.org/)
* [DOOM ONLINE!!!](https://zandronum.com/)
* [Rocket league - D9VK](https://www.gamingonlinux.com/articles/a-three-way-look-at-rocket-league-on-linux-with-d9vk-versus-linux-native.14718)

* [My buddy Matt (awesome artist) is working on a game](https://www.kickstarter.com/projects/theropods/theropods-an-adventure-game-full-of-dinosaurs)

### ArchToasty
* [Hotline Miami](https://store.steampowered.com/app/219150/Hotline_Miami/)
* [Overwatch](https://playoverwatch.com/en-us/)

### Not News... 

* [Remember that LUTRIS FLATPAK talk?](https://www.forbes.com/sites/jasonevangelho/2019/02/08/lutris-is-working-on-snap-and-flatpak-installers-for-linux-gamers/#7e294b6ee0d0) *this is old news, yes.. but i tried it recently*
    * [Here is how](https://github.com/flathub/net.lutris.Lutris)
* [Blender 2.80](https://www.blender.org/download/releases/2-80/)
* [Valve want some Kernel changes to make (wine?) gaming better](https://lkml.org/lkml/2019/7/30/1399) *i dont understand any of this*
* [FRESH PROTON!](https://github.com/ValveSoftware/Proton/wiki/Changelog#411-1)
* [MORE WHITE LISTED GAMES!](https://steamdb.info/app/891390/history/?changeid=6655488)
* [Steam-DOS changed its name and had a new release](https://www.reddit.com/r/linux_gaming/comments/ckcd7p/steamdos_renamed_as_boxtron_for_its_050_release/) *reddit page is good outline of stuff*
* [need new name?](https://www.mess.be/inickgenwuname.php)

## Contact us!

### ArchToasty

* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Twitch](http://twitch.tv/archtoasty)
* [Mastodon](@archtoasty@linuxrocks.online)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)
  
## On Youtube
{% include yt.html vidid="hrB1AJdkwJg" %}
