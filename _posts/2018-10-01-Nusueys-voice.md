This week Hex is joined by LGG Mod, Nusuey! (LGG mod, TuxDB Creator, Streamer and Guitar Legend!) Where we discuss that one Linus Tech Tips video, the new POL release and other such wonderful newsy type items. We also praise CrossCode and talk Battle(rite) Royale! 
<!--more--> 

## Host Information:

### HexDSL 

**Games:** 

* [CrossCode](https://store.steampowered.com/app/368340/CrossCode/)
* [Log Jammers](https://store.steampowered.com/app/684800/Log_Jammers/)

**This week:** 

FYI! Turns out that Bluetooth 'scanning' *can* break Suspend! (good work systemd, you knob!)

PSA: To the people messaging me about OVERWATCH not running okay, IDK why yours isn't working but I'm running at like 70fps dead on, its not a trick, I streamed it more than once, so have other people! (yes, I play a lot! I'm lvl 85 100% Linux time logged!)

### NuSuey!

**Games:** 

* [RockSmith 2014](https://store.steampowered.com/app/221680)
* [Battlerite Royale](https://store.steampowered.com/app/879160)
* [Book of Demons](https://store.steampowered.com/app/449960)

**This week:** 

Played too many non-native games .. but not ashamed of it!
Tried to make ALL WALLS MUST FALL working (freezes the entire pc on my system), tried bootable LIVE usb, same thing, tried using LLVM7 from the ARCH staging repos (they even tell you on the arch wiki to not use them) same thing.
Lesson learned: Probably should just bug report instead
    
## Not news!

* [Feral as a BIG 'thing' Coming](https://www.gamingonlinux.com/articles/feral-interactive-are-teasing-something-for-linux-next-week.12645)
* [LTT finally made a *good?* Linux Video](https://www.youtube.com/watch?v=IWJUphbYnpg&feature=youtu.be)
* [PlayOnLinux 5.0 alpha released](https://www.playonlinux.com/en/comments-1354.html)
* [That Steam Controller blog](https://steamcommunity.com/games/593110/announcements/detail/1712946892833213377)
* [Dox updated the Blizzard installer on Lutris](https://www.reddit.com/r/linux_gaming/comments/9j1qi5/lutris_blizzard_games_septoct_update_esync_now/)
* [Wanna know if a game runs in Proton?](https://addons.mozilla.org/en-CA/firefox/addon/spcr-for-steam/) (HexDSL does not endorse the use of browser extensions)
* [New Proton Beta](https://github.com/ValveSoftware/Proton/blob/proton_3.7/CHANGELOG.md#37-7)


## Contact us!

### NuSuey

* [Twitch](http://twitch.tv/nusuey)
* [YouTube](https://www.youtube.com/user/NuSuey)
* [The LGG](https://tuxdb.com/lgg)
* [TuxDB](https://tuxdb.com)
* [TuxDB Patreon](https://www.patreon.com/tuxdb)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube 
{% include pt.html vidid="2ba36f25-c31c-45f6-8334-16bb8a43d0cb" %} 
