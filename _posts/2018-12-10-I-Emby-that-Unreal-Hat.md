Hex is still obsessed with Artifact and because of that, hasn't played a lot else. He did however have a lot of opinions about Artifact, gene shift, CS:GO and steam reviews. He also talked about Steam Link on the Pi, GGKBDD, WPF being open source as well as some anger aimed at the Emby project.
<!--more--> 

### HexDSL

**Games:** 

* [Artifact](https://store.steampowered.com/app/583950/Artifact/)
* [Gene Shift](https://store.steampowered.com/app/308600/Geneshift/)

## The Not News! 

* [Steam Link + pie = gud?](https://steamcommunity.com/app/353380/discussions/0/1743353164093954254/)
* [Epic Store](https://www.unrealengine.com/en-US/blog/announcing-the-epic-games-store)
* [Battle Royale - CS:GO!](http://counter-strike.net/dangerzone) also its free now!!!
* [GG KB d](https://www.phoronix.com/scan.php?page=news_item&px=Linux-GGKBDD-Daemon) - new cheating tool?
* [Microsoft open source WPF (and other stuff)](https://blogs.windows.com/buildingapps/2018/12/04/announcing-open-source-of-wpf-windows-forms-and-winui-at-microsoft-connect-2018/) - Good news for all those launchers?
* [UE4 now has VULKAN as default on Linux](https://twitter.com/UnrealEngine/status/1071075662310588417)  hex dont care. 
* [Emby media server is now prop'](https://github.com/MediaBrowser/Emby/issues/3479#issuecomment-444985456)
* [A Hat in Time is coming](https://www.gamingonlinux.com/articles/looks-like-both-a-hat-in-time-and-gravel-are-coming-to-linux-ports-from-virtual-programming.13114)

## Contact us!


### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube
{% include pt.html vidid="288e06df-8c6e-4f80-a4dd-44bb155f5a4f" %}
