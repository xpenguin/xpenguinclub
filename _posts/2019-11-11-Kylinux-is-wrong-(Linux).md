Hex is joined by KyLinux. Hex then has to tell Ky why he is wrong for an hour. Good times
<!--more--> 

```( o Y o )```


[A onetab link](https://www.one-tab.com/page/wLBeoCDhSYuaF3qTbmowfg)

### HexDSL

* [Star Traders: Frontiers](https://store.steampowered.com/app/335620/Star_Traders_Frontiers/) *good*
* [Vulture for NetHack](https://store.steampowered.com/app/341390/Vulture_for_NetHack/) *Very Bad*
* [Clicker bAdventure](https://store.steampowered.com/app/736430/Clicker_bAdventure/) *very Bad*
* [Toejam and Earl. its shit](https://store.steampowered.com/app/516110/ToeJam__Earl_Back_in_the_Groove/) *very Bad*

### KyLinux

* Companies who port Linux games are still beneficial to Linux adoption despite Proton
    * Game updates break games in Proton occasionally
    * No guarantee patches will be made to allow games to run
    * (Some) Companies (usually) hold themselves accountable for LTS
    * Users switching to Linux want to know they have LTS from developers

> 💬 **This is Hex:** Everything Ky has written above is nonsense.

* [Sekiro (completed)](https://store.steampowered.com/app/814380/Sekiro_Shadows_Die_Twice/)
* [Shadow of the Tomb Raider (Feral's port)](https://store.steampowered.com/app/750920/Shadow_of_the_Tomb_Raider_Definitive_Edition/)
* [Hitman: Blood Money -- soooo damn good!!! (thank you Chris Were)](https://store.steampowered.com/app/6860/Hitman_Blood_Money/)

### Not The News

* [Steam Small mode is back!](http://www.pcgamer.com/you-can-now-change-your-steam-library-view-back-to-small-mode)
* [Steam Cloud Gaming](https://www.gamingonlinux.com/articles/looks-like-valve-could-be-set-to-launch-something-called-steam-cloud-gaming.15354)

[Read this about rogue likes. Glog78 said its good!](http://www.roguebasin.com/index.php?title=Berlin_Interpretation)

## Contact us:

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [My Website](http://hexdsl.co.uk)
* [.XPenguin Website](http://xpenguin.club)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

### KyLinux

* [Ky's YouTube](https://www.youtube.com/kylinuxcast)
* [Ky's Twitch](https://www.twitch.tv/kylinuxcast)

## On Youtube
{% include yt.html vidid="gXrDugz3vgo" %}# 
