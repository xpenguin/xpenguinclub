This week on Xpenguin Hex talks Outward... a lot of Outward. There is a lot of Linux gaming showing up on in mainstream media to talk about too.... 
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)
* [Octopath Traveler](https://www.humblebundle.com/store/octopath-traveler?partner=hexdsl) 
* [Outward](https://www.humblebundle.com/store/outward?partner=hexdsl) *hard as balls but great*
* [DOTA Underlords](https://store.steampowered.com/app/1046930/Dota_Underlords/) *multiplayer is methodical fun*
* [Quadriga](https://store.steampowered.com/app/297760/Qvadriga) *I cant see why Drew likes it, but damn, its shite looking and janky slow... but charming.* 

## The Not News!

* [DJAZZ made Celeste work on Raspberry pi](https://www.youtube.com/watch?v=iTBUNb6IKHo)
    * [Based on this article](https://magazine.odroid.com/article/playing-modern-fna-games-on-the-odroid-platform/)
* [valve to support more diatros?](https://fossbytes.com/valve-steam-work-with-linux-distros-gaming/)
* [PC GAMER is talking linux](https://www.pcgamer.com/how-to-play-windows-games-in-linux/)
* [RPS reporting on Ubuntu 32 bit debate](https://www.rockpapershotgun.com/2019/06/24/steam-linux-likely-to-not-support-future-versions-of-ubuntu/)
* [Text to Speach in retrparch](https://www.patreon.com/posts/retroarch-in-to-27885187) *early, but super interesting* 
* [Run steam in a container, without flatpak, with nspawn](https://www.reddit.com/r/linux_gaming/comments/c50vsb/howto_run_steam_in_a_container_without_32bit/) *good guide on reddit, Uoou also wrote on on ludiclinux.com*

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="HYuSE2RaW9I" %}
