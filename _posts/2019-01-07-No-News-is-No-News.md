2019: Year of the Linux Desktop, or not. 
<!--more--> 

### HexDSL

**Games:** 

* [Ni No Kuni II](https://store.steampowered.com/app/589360/Ni_no_Kuni_II_Revenant_Kingdom/) *Proton* 
* [Horizon Chase Turbo](https://store.steampowered.com/app/389140/Horizon_Chase_Turbo/)
* [BallisticNG](https://store.steampowered.com/app/473770/BallisticNG/)
* [Carcassonne Tiles & Tactics](https://store.steampowered.com/app/598810/Carcassonne__Tiles__Tactics/) *Proton*

Also... [I like groff](https://www.reddit.com/r/groff/) and no one who isn't a tit seems to be pushing it, so I made a Subreddit for grownups.

Channel thoughts, should I do more 'formal' game videos or keep on wearing a dressing gown and rambling shite? - Also, Groff Tutorials? 

## The Not News! 

* [Wimpys World](https://www.youtube.com/watch?v=ONj1EJirsu0)
* [Boiling Steam](https://boilingsteam.com/theres-no-such-thing-as-linux-games/)
* [Feudal Alloy - Released soon!](https://store.steampowered.com/app/699670/Feudal_Alloy/) *I promise to have a Video!*
* [7 things Linux needs](https://www.linux.com/blog/2019/1/7-things-desktop-linux-needs-2019)
* [Godot 3.1 on the way!](https://www.gamingonlinux.com/articles/the-free-and-open-source-game-engine-godot-engine-is-closing-in-on-the-big-31-release.13270)

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="9QjYX3sLVuU" %}
