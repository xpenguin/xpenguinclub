.XPenguin tackles the big issues... like living with Factorio Obsession! and Stadia... and stuff 
<!--more--> 

### HexDSL

**Games:** 

* [Factorio](https://store.steampowered.com/app/427520/Factorio/)
* [Space Mining Clicker](https://store.steampowered.com/app/1027980/Space_mining_clicker/)
* [Dungeons 3](https://store.steampowered.com/app/493900/Dungeons_3/)*multiplayer*

## The Not News!

* [steam Changes Requirements for versions](https://twitter.com/Plagman2/status/1107693164591087616)
* [DWARF FORTRESS COMING TO STEAM!](http://www.bay12games.com/dwarves/)
    * [Steam Page](https://store.steampowered.com/app/975370/Dwarf_Fortress/)
* [Battle for Wesnoth porting to GODOT!](https://www.gamingonlinux.com/articles/looks-like-battle-for-wesnoth-is-being-ported-to-godot-engine.13774)
* [The Culling, gone](http://theculling.com/blog/2019/3/18/dev-diary-so-long-farewell) 
* [Use Your Old GameCube Controllers With SDL2](https://www.phoronix.com/scan.php?page=news_item&px=GameCube-Adapter-SDL2-Support)
* [System Shock remaster updates](https://www.gamingonlinux.com/articles/nightdive-studios-have-released-more-footage-of-their-system-shock-reboot.13796) 
* [Albion online goes free way after we expected](https://albiononline.com/en/news/albion-online-goes-free-to-play)
* [Steam Update](https://www.gamingonlinux.com/articles/valve-show-off-their-new-steam-library-design-and-a-new-events-page.13809)
* [So, i guess that i have to talk about Stadia then](http://stadia.com/)
* [New Lutris Version](https://github.com/lutris/lutris/releases/tag/v0.5.1)

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="cNMgz80HMIs" %}
