ArchToasty talks about naked dog baths and gets pooped on by a cat. Yes... really. Meanwhile Hex talks Stadia, Ys games and we have a a good old rant about text editors! :P 
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)

* [Kingdoms of Amular](https://store.steampowered.com/app/102500/Kingdoms_of_Amalur_Reckoning/)
* [Ys Origin](https://store.steampowered.com/app/207350/) *Thanks KyLinux* 
* [The Sims 3 WITH MODS!](https://store.steampowered.com/app/47890/) *thanks JumbleSailer⚓*

### ArchToastBoi
* [Battlerite](https://store.steampowered.com/app/504370/Battlerite/)
* [Savage Lands](https://store.steampowered.com/app/307880/Savage_Lands/)

### Not News... 

* [Forbes article about stuff](https://www.forbes.com/sites/jasonevangelho/2019/07/17/these-windows-10-vs-pop-os-benchmarks-reveal-a-surprising-truth-about-linux-gaming-performance/amp/)  *"Valve developer Pierre-Loup Griffais told me that the performance work being done in Proton and DXVK has gotten to a point where the CPU overhead of translating DX11 through DXVK can be lower than the overhead in the native AMD DX11 driver on Windows"*
* [Steam Updates](https://store.steampowered.com/news/52467/)
* [Valve Citidel????](https://www.gamingonlinux.com/articles/looks-like-valve-are-developing-another-new-game-something-to-do-with-citadel.14608)
* [Ubi soft say linux usnt that expensive](https://www.reddit.com/r/linux_gaming/comments/cfc6rr/ubisoft_exec_admits_that_development_cost_for/)
* [STADIA AMA](https://www.reddit.com/r/Stadia/comments/ceuy4w/hi_im_andrey_doronichev_and_im_the_director_of/) *when asked about a partnership with Valve, the answer was "Great question! My PR guy will kill me...we’re always evaluating our options to make Stadia a better place for the gamers :)" Also the Chomuim vs chrome question was ignored* 

## Contact us!

### ArchToasty

* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Twitch](http://twitch.tv/archtoasty)
* [Mastodon](@archtoasty@linuxrocks.online)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)yy
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)
  
## On Youtube
{% include yt.html vidid="Cs5QGaK3T3Q" %}
