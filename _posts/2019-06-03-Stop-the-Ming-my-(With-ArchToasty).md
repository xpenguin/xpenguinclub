Hex and Toasty talk about gaming! - this week featuring all this stuff, Hex DIDNT play :) - Dont worry though, we DO talk about *that* Microsoft news.
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)

 * [Seal Fell](https://www.humblebundle.com/store/fell-seal-arbiters-mark?partner=hexdsl)
 * [RUSTY LAKE](https://www.humblebundle.com/store/rusty-lake-hotel?partner=hexdsl)

*yes, thats all! ive been busy!*

### Toasty

**Games:** 
* [Lover in a Dangerous Spacetime](https://store.steampowered.com/app/252110/Lovers_in_a_Dangerous_Spacetime/)
* [Mana Spark](https://store.steampowered.com/app/630720/Mana_Spark/)
* [Guild Wars 2](https://www.guildwars2.com)

## The Not News!

* [GOG 2.0 OPEN SOURCE!](https://youtu.be/kvnOXIiJrMw?t=155)
* [UNITY EDITOR FOR LINUX, out of beta](https://blogs.unity3d.com/2019/05/30/announcing-the-unity-editor-for-linux/)
* [Microsoft bringing more games to steam](https://www.pcgamer.com/microsoft-games-on-steam-announced/?utm_content=buffer8b1ef&utm_medium=social&utm_source=facebook&utm_campaign=buffer_pcgamerfb) *response to Epic Exclusives?*
* [Stop Theming My App website is a thing](https://stopthemingmy.app) *Hex strongly Disagrees* 
* [A video about turning your old TV sat into something awesome!](https://invidio.us/watch?v=8iAkIxEk7g8)
 
## Contact us!

### ArchToasty 

* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Twitch](https://www.twitch.tv/archtoasty)
* [Mastodon](https://linuxrocks.online/@archtoasty)
* [Email](archtoasty@protonmail.com)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="cvsYdqkTbug" %}
