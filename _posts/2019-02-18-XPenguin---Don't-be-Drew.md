This week Hex is joined by Drew, so there is a lot of off topic rambling and some retro thoughts about handhelds. Also we talk Rocket League, Katamari, Titan Quest and Dicey Dungeons... This and more in another Fun Packed episode! 
<!--more--> 

### HexDSL

Hex's *EPIC* Controller issues! (my controller stopped working entirely in Proton and in Native Rocket league!) 

Hex ordered the 

**Games:** 

* [Katamari Demacay](https://store.steampowered.com/app/848350/Katamari_Damacy_REROLL/)
* [Hard Reset: Redux](https://store.steampowered.com/app/407810/Hard_Reset_Redux/)

### uoou

Drew is bored of games but having a lot of fun with Linux.

**Games:**

* [Titan Quest](https://store.steampowered.com/app/475150/Titan_Quest_Anniversary_Edition/)
* [Dicey Dungeons](https://terrycavanagh.itch.io/dicey-dungeons)

## The Not News! 

* [Rocket lague cross platform](https://www.gamingonlinux.com/articles/rocket-league-to-get-a-fully-cross-platform-friends-system-next-week-along-with-the-next-season.13559)
* [EA GAMES COMING TO LINUX. NOT REALLY! PSYCH!](https://www.reddit.com/r/linux_gaming/comments/apwmco/i_guess_ea_is_working_on_a_linux_origin_client/)
* [Ethan Lee wants to make Nintendo GameCube Adapter work on everything](https://www.gamingonlinux.com/articles/game-porter-and-steam-play-dev-ethan-lee-is-running-a-crowdfunding-campaign.13564)
* [PROTON 3.16 IS OUT](https://github.com/ValveSoftware/Proton/wiki/Changelog#316-7)

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

### uoou *(Drew, Uoniss, Friendo)*

* [Ludic Linux](http://ludiclinux.com)
* [Mastodon](https://linuxrocks.online/@uoou)

## On Youtube
{% include yt.html vidid="YGRvnAmHm7Y" %}
