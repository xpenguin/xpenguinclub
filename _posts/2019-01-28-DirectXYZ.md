This week we talk and talk and talk about DirectX-wine stuff. Ion Maiden updates, Artifact becoming an Artifact and my endless opinions about Metroidvania games. 
<!--more--> 

### HexDSL

**Games:** 

Shout out to https://www.youtube.com/channel/UC6lk8p4SAYUoJeIymAqGALA (Vipor29 Gaming)
 
* [Stellaris](https://store.steampowered.com/app/281990/Stellaris/) *Native*
* [Paladins](https://store.steampowered.com/app/444090/Paladins/) *Proton*
* [Feudal alloy](https://store.steampowered.com/app/699670/Feudal_Alloy/) *Native* 

Hex moved to BSPWM and its 'okay' 

## The Not News! 

* [Artifact soon to be an artifact](https://www.gamingonlinux.com/articles/valves-card-game-artifact-seems-to-be-dying-off-and-fairly-quickly-too.13413)
* [Wine is working on a Vulkan Dx9 thingy](https://www.phoronix.com/scan.php?page=news_item&px=WineD3D-Vulkan-Exploring)  I mean... Gallium Nine - VK9 - WineD3D - DXUP - When de we have enough?
* [Wine wont use DXVK because emails?](https://www.gamingonlinux.com/articles/some-information-on-why-wine-is-not-going-to-be-using-dxvk.13441)
* [Wine spills the tea](https://www.gamingonlinux.com/articles/some-information-on-why-wine-is-not-going-to-be-using-dxvk.13441)
* [Slay the Spire is now Launched!](https://www.youtube.com/watch?v=9SZUtyYSOjQ) an EA success!
* [Ion Maiden Updates!](https://store.steampowered.com/app/562860/Ion_Maiden/) I bloody love this game, and its not even out yet! 
* [Wine 4.0 is fully cooked](https://www.reddit.com/r/linux_gaming/comments/aisjw9/wine_40_released/) the interesting thing here is all the mumbling about Valve negotiating with EAC to have steam white listed, is this how Paladins suddenly started working?

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="0PI7-KyqD3M" %}
