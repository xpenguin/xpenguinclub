This week im joined by Drew (Uoou) who brings his signature brand of salt and Nintendo talk. We also talk EndevourOS, Crazy chineese laptops and we are oddly in pro Super Tux Kart for once! - this was good one. 
<!--more--> 

### HexDSL

* [Ys 1 (Chronicles)](https://store.steampowered.com/app/223810/Ys_I__II_Chronicles/)
* [Switched to flatpak of lutris because Nspawn stressed me out](https://github.com/flathub/net.lutris.Lutris)

### uoou 

* [Steamworld Dig 1 & 2](https://store.steampowered.com/search/?term=steamworld%20dig)
* Golf Story
* [Golf Peaks](https://store.steampowered.com/app/923260/Golf_Peaks/)

### Not News...

* [NS2 leaving linux](https://unknownworlds.com/ns2/a-note-to-our-linux-users/) *Wonder if i get a refund.*
* [Endeavour OS updates](https://endeavouros.com/news/get-ready-for-the-next-level/)
* [Discord GO LIVE not coming to Linux](https://www.gamingonlinux.com/articles/seems-like-discords-new-go-live-feature-is-not-coming-to-the-linux-version.14987)
* [Huawei going Linux](https://www.pcmag.com/news/370748/huawei-opts-for-linux-on-its-laptops)

## Contact Me

### uoou (LINKS)

* [Myspace profile](https://www.patreon.com/hexdsl)

### HexDSL *(Sexy Hexy)*

* [MY WEBSITE](http://hexdsl.co.uk)
* [MY Art Website (Yes, Really)](http://pixelfridge.club)
* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

## On Youtube
{% include yt.html vidid="u7lprYxo9cE" %}
