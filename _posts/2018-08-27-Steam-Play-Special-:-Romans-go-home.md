The only topic this week is STEAM PLAY because its the only news Hex cares about :) - Windows games on Linux via steam, one click wine is here! - Hex is joined my both Ouou, ArchToasty & Coda, we discuss and debate what it all means! (also, we learn what the number XV is) 
<!--more--> 

## Host Information:

* HexDSL
* Coda
* uoou
* ArchTaosty

## STEAM PLAY! 

This is the only topic we need for this week! - work down the list in order, try not to get distracted. when we reach the end we do the open discussions.

* [Announcement](https://steamcommunity.com/games/221410/announcements/detail/1696055855739350561)
    * Wine with esync patches
    * DXVK
    * VK3D
    * Some wine passthrough magic
    * "Fullscreen support has been improved: fullscreen games will be seamlessly stretched to the desired display without interfering with the native  monitor resolution or requiring the use of a virtual desktop."

* [Reported on by FORBES](https://www.forbes.com/sites/jasonevangelho/2018/08/22/valve-changes-everything-windows-exclusive-games-now-run-on-steam-for-linux/#12f672cce3fa)
* [Official statement from code weavers](https://www.codeweavers.com/about/blogs/jwhite/2018/8/22/wine-and-steam-a-major-milestone) - They were in on it! 
* [Icculus says it all!](https://twitter.com/icculus/status/1032127387293638656)
* [AUR](https://aur.archlinux.org/packages/proton/)
* [over 100 issues on Github! - mostly responded to!](https://github.com/ValveSoftware/Proton/)

* [shipped with DEBUG mode enabled on DXVK, now resolved](https://twitter.com/Plagman2/status/1032450256087044096)
* [Reddit community made a 'test list'](https://docs.google.com/spreadsheets/d/1DcZZQ4HL_Ol969UbXJmFG8TzOHNnHoj8Q1f8DIFe8-8/htmlview?sle=true#gid=0)
* [Reddit /r/SteamPlay has a wiki](https://old.reddit.com/r/ProtonForSteam/wiki/index)
* [Carmack was wise indeed](https://boilingsteam.com/carmack-the-wise/)

* **Tip:** Steam-play winecfg use - *WINEPREFIX=/path/to/steamapps/compatdata/gameid/pfx/ winecfg*
* **Tip:** Python 2.7 is required to use Proton
* **Tip:** SteamOS has no steamplay options, to work around exit to desktop mode, sudo -i -u steam, pkill steam, reload enable options, restart.

## Contact me!

### TOASTY *(The Toast with the Most)*

* [Twitch](https://www.twitch.tv/archtoasty)
* [twitter](https://twitter.com/archtoasty)
* [Mastodon](@archtoasty@linuxrocks.online)
* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Email](archtoasty@pm.me)

### CODA *(code ahhhh)*

* [Twitch](https://www.twitch.tv/jay_coda)

### uoou *(ooohohoauou)*

* [Mastodon](https://linuxrocks.online/@uoou)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/video-channels/63feea73-94f9-4fb6-93f2-8db11a9eec63/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://mastodon.rocks/@HexDSL)
* [Twitter](https://twitter.com/HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube
{% include pt.html vidid="482b1082-4b10-4eb4-808d-ade9399116a3" %}
