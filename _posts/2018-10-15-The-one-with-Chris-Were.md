This week Hex is joined by well known YouTuber Chis Were to talk about Hitman™, Mini Metro and Long Live the Queen. Hex praises XCOM 2 (again!) and we show some love for the BBC computers TV show archive. 
<!--more--> 

## Host Information:

### HexDSL 

**Games:** 

* [XCOM2: WotC *NEW DLC!*](https://store.steampowered.com/app/268500/XCOM_2/) (see below for link)
* [*I Tried to play* Battletech](https://store.steampowered.com/app/637090/BattleTech) - It closes! 

**This week:** 

I released the first in a new video series ([Let's Watch!](https://www.youtube.com/watch?v=bo6SDSCW9qg)) where I talk shit wile watching TV shows! (first one was Star Trek TOS)

### ChrisWere

**Games:** 

* [Long Live the Queen](https://store.steampowered.com/app/251990/Long_Live_The_Queen/)
* [HITMAN™](https://store.steampowered.com/app/236870/HITMAN/)
* [Mini Metro](https://store.steampowered.com/app/287980/Mini_Metro/)
* [Gunpoint](https://store.steampowered.com/app/206190/Gunpoint/)

**This week:** 

## Not news!

* [XCOM DLC, same day as windows release](https://www.youtube.com/watch?v=AkI76SAOK-U)
* [Helium Rain leaves Early Access](https://steamcommunity.com/games/681330/announcements/detail/2618171872226612271)
    * [Developer post on reddit](https://www.reddit.com/r/linux_gaming/comments/9ndd34/helium_rain_indie_space_sim_released_with_linux/)
    * [Also on Itch.io](https://deimos-games.itch.io/helium-rain)
* [The Away Team update](http://awayteam.space/press-release_2018-10-06.php)
* [Unreal Engine updates](https://www.gamingonlinux.com/articles/epic-games-have-rolled-out-unreal-engine-421-preview-with-linux-improvements.12733)
* [Found this BBC archive of old shows that I thougth you would all enjoy](https://computer-literacy-project.pilots.bbcconnectedstudio.co.uk/)

Not gaming related but I found [this article about Abiword](https://www.dedoimedo.com/computers/abiword-2018.html), I remember how much I liked it a few years ago, thought I would mention it. As its still a thing and all.

## Contact us!

### ChrisWere

* [Patreon](https://www.patreon.com/ChrisWere)
* [YouTube](http://youtube.com/user/ChrisWereDigital)
* [PeerTube](https://share.tube/accounts/chriswere)
* [Twitch](http://twitch.tv/ChrisWere)
* [Mastodon](https://linuxrocks.online/@ChrisWere)

### HexDSL

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube
{% include pt.html vidid="d9c267f1-2157-4fe4-ac05-d8950cecc48c" %}
