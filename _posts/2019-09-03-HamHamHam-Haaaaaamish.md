HAM HAM HAM HAM HAMIIIIIIIIISH
<!--more--> 

### HexDSL

* [OverDungeon (refunded it, it was not for Hex!)](https://store.steampowered.com/app/919370)
* [Shining Force (Android)](https://play.google.com/store/apps/details?id=com.sega.mega&hl=en_GB)
* [WarFork](https://store.steampowered.com/app/671610)
* [Astral Chain (Switch)](https://www.youtube.com/watch?v=brmmV3g4qqo)

* [This book is great! - Off to be the wizard](https://www.goodreads.com/book/show/18616975-off-to-be-the-wizard)
### HAMISH

FOSS Games Week on Stream
Almost all distro repos have these available

(Some highlights)
* [Armagetron Advanced](http://www.armagetronad.org/)
* [Battle for Wesnoth](https://www.wesnoth.org/)
* [Pushover](https://pushover.github.io/)
* [Frogatto & Friends](https://frogatto.com/) *Data is copyright but engine and source are open*

Not games:

* [Codeberg](https://www.codeberg.org)
* [Startpage.com](https://www.startpage.com/) [*(That video Hex made about search engines)*](https://youtu.be/WR6S4XBwX8M)
* [Privacy Tools](https://www.privacytools.io/)
* New monthly show starting tomorrow.

### Not News... 
* [war thunder adds EAC](https://warthunder.com/en/news/6347-news-easyanticheat-comes-to-war-thunder-en) No More Proton!
      * [War Thunder Dev things we should be grateful!](https://www.reddit.com/r/linux_gaming/comments/cukgio/be_thankful_there_is_linux_version_at_all_war/) - and Lets not forget that from last week!
* [0 to 6000, forbes talk one year of proton](https://www.forbes.com/sites/jasonevangelho/2019/08/22/from-0-to-6000-celebrating-one-year-of-valves-genius-linux-gaming-solution/#5bd8e09e1eaa) may stop talking about forbes articles, feel a bit like its not insightful, just sort of states things we all know and has odd popups.
* [Everspace 2 - Linux Native Confirmed](https://www.terminals.io/product/?pid=609) *also confirmed by Developer on Reddit AND Discord 
* [New GE-Proton! (4.14-GE-2)](https://github.com/GloriousEggroll/proton-ge-custom/releases/tag/4.14-GE-2)

## Contact us!

### HAMISH CONTACTS

* [Website](https://thepolarbear.co.uk)
* [Twitch](https://twitch.tv/hamishtpb)
* [Mastodon](https://linuxrocks.online/@HamishTPB)
* [Email](hamish@thepolarbear.co.uk)
* [YouTube](https://www.youtube.com/channel/UCp1mWfjYbMcmNowBmvTUCag)
* [PeerTube](https://peertube.linuxrocks.online/accounts/hamishtpb/videos)
* Matrix @HamishTPB:matrix.org #polarbears:matrix.org

### HexDSL *(Sexy Hexy)*

* [MY WEBSITE](http://hexdsl.co.uk)
* [MY Art Website (Yes, Really)](http://pixelfridge.club)
* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

## On Youtube
{% include yt.html vidid="dIBWPv0JJKc" %}
