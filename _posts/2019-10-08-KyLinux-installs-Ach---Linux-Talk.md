Hex is joined by Kylinux this week. We hear about his Arch Installation Nightmares. Hex Played Littlwood and renamed things to make it way less wholesome (obviously!) and we also talk about Bungie being hats for rear ends when it comes to Proton Support.
<!--more--> 


### HexDSL games

* [MINDUSTRY!](https://store.steampowered.com/app/1127400/Mindustry/) Played More, got a bit bored *(Native)* 
* [Littlewood](https://store.steampowered.com/app/894940/Littlewood/) Slow, Grindy, Great! *(Native)*
* [Risk of Rain 2](https://store.steampowered.com/app/632360/Risk_of_Rain_2/) Still fun, needs an an ending. *(proton)*

* *[Update/reminder about BLOP blog system](https://gitlab.com/uoou/blop) Thanks Drew! - again*

### Kylinux games

* [INSTALLED ARCH BADLY](https://wiki.archlinux.org/index.php/installation_guide)
* [FINAL FANTASY XIV](https://store.steampowered.com/app/632360/Risk_of_Rain_2/) IS BEST GAME *INSTALL IT, HEX. DO IT!!!*

### Not News... 

* [Bungie ban your for playing Destiny 2 on Linux?](https://i.redd.it/tzj1c1qt2dq31.png)
* [Forager Dropping Linux support](https://old.reddit.com/r/linux_gaming/comments/dd3dc4/the_linux_version_of_forager_is_being_discontinued/)
* [Athenaeum: A libre launcher](https://www.reddit.com/r/Athenaeum/comments/dc69r2/athenaeum_v10_is_released/) *Hex doesnt understand why people would use this*
* [TitanQuest Multiplayer (proton) fixed, finally](https://www.reddit.com/r/linux_gaming/comments/db9gr1/fix_for_titan_quest_multiplayer_for_proton/) *community is awesome!* 

## Contact!

### KYLINUX

* [YouTube](https://youtube.com/kylinuxcast)
* [Mastodon](https://linuxrocks.online/@KylinuxCast)
* [Twitch](http://twitch.tv/KylinuxCast)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [My Website](http://hexdsl.co.uk)
* [My Art Blog (Yes, Really)](http://pixelfridge.club)
* [.XPenguin Website](http://xpenguin.club)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)
  
## On Youtube
{% include yt.html vidid="CuTmiBB0wC8" %}
