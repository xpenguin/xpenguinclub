This week Hex Talks about his love affair with vim, an article wot he did write about clickers wiv hiz ace english skillzorz, how Observer was ruined by the strange, some Morrowind love and then some news type stuff...
<!--more--> 

## Host Information:

### HexDSL 

**Games:** 

* Vim - Its like a game at this point, I clicked Esc at work today to leave Insert mode. I was in MS word
* [Oberver](https://store.steampowered.com/app/514900/observer/)
* [Bloody Rally Simulator](https://store.steampowered.com/app/926860/Bloody_Rally_Simulator/)
* [Morrowind](https://store.steampowered.com/app/22320/The_Elder_Scrolls_III_Morrowind_Game_of_the_Year_Edition/) *also OpenMW*

## Not news!

* [THE FORUM!](http://forum.xpenguin.club)
* [Redhat, Dreamhack](https://www.redhat.com/en/blog/talk-us-about-open-source-gaming-dreamhack-atlanta-2018)
    * [Toastys Postys](https://www.reddit.com/r/linux_gaming/comments/9x08z9/red_hat_is_going_to_dreamhack/)
* [Reddit user Ported Deltarune himself](https://www.reddit.com/r/Deltarune/comments/9wizh3/deltarune_running_on_linux_natively/) *Aliens!*
* [Feral radar](https://www.reddit.com/r/linux_gaming/comments/9xcglq/new_game_on_the_feral_radar/)
* [SteamPlay Whitelist updated](https://www.gamingonlinux.com/articles/valve-has-expanded-the-steam-play-whitelist-to-include-dark-souls-iii-and-plenty-more.12963) *Dark Souls III* 
* [Total War: Warhammer 2 - 20th Nov!](https://store.feralinteractive.com/it/mac-linux-games/warhammer2tw/)
* [This war of mine DLC *is* now coming to Linux](https://www.gamingonlinux.com/articles/11-bit-studios-actually-will-put-the-latest-this-war-of-mine-dlc-on-linux.12971) well, that was confusing.

* [Elecom Huge is down to £65!](https://www.amazon.co.uk/ELECOM-Wireless-trackball-function-M-HT1DRBK-Black/dp/B0735584RM/ref=sr_1_1?ie=UTF8&qid=1542267172&sr=8-1&keywords=elecom+huge) *NOT an affiliate link*

## Contact me!

### HexDSL

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)


## On Peertube
{% include pt.html vidid="efd0c63e-2484-4e3f-b13a-d9258881837d" %}
