Hex and Chris Were talk about Linux, games and octopus attacks. 
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)

* [Monster Hunter World](https://store.steampowered.com/app/582010/MONSTER_HUNTER_WORLD/)
* [Octogeddon](https://store.steampowered.com/app/525620/Octogeddon/)
* [Iron cast](https://store.steampowered.com/app/327670/Ironcast/)

Hex used his steam controller for a week

### Chris Were

* [Hitman™ 2](https://store.steampowered.com/app/863550/HITMAN_2/)
* [Hitman: Blood Money](https://store.steampowered.com/app/6860/Hitman_Blood_Money/)
* [Vampire: the Masquerade - Bloodline](https://www.gog.com/game/vampire_the_masquerade_bloodlines)
* [Neon Struct (free demo available)](https://store.steampowered.com/app/310740/NEON_STRUCT/)

### Not News... 

* Do we actually NEED the news section of this show? 
* [Ion Fury](https://www.gamingonlinux.com/articles/ion-maiden-has-become-ion-fury-release-date-announced-for-august-15th.14554)
* [New kdenlive](https://www.omgubuntu.co.uk/2019/07/kdenlive-70-bug-fixes)
* [Steam Labs](https://store.steampowered.com/labs/)
* [RetroArch coming to STEAM!](https://store.steampowered.com/app/1118310/RetroArch/)
* [Great guide over on GOL for new Proton users!](https://www.gamingonlinux.com/articles/a-simple-guide-to-steam-play-valves-technology-for-playing-windows-games-on-linux.14552)
 
## Contact us!

### Chris Were 

* [Website](https://ChrisWere.uk)
* [Twitch](https://twitch.tv/ChrisWere)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)yy
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)
  
## On Youtube
{% include yt.html vidid="je-xANpSeqY" %}
