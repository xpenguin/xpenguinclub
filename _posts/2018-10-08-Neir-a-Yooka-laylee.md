This week Hex is joined by uoou, the discussion ranges from how great Nier: Automata is to some unexpected praise for Yooka-Laylee. There is also a stop off to talk CrossCode and The Gardens Between as well as few more games. This weeks discussion items include Feral Interactive's recent announcements, some new Steam Play white list games and more...
<!--more--> 

## Host Information:

### HexDSL 
    
**Games:** 

* [CrossCode](https://store.steampowered.com/app/368340/CrossCode/)
* [yooka-laylee](https://store.steampowered.com/app/360830/)

**This week:** 

### uoou
    
**Games:**

* [Nier: Automata](https://store.steampowered.com/app/524220/NieRAutomata/)
* [The Gardens Between](https://store.steampowered.com/app/600990/The_Gardens_Between/)
    * [Ludiclinux article](http://ludiclinux.com/The-Gardens-Between/)
* [Tabletop Sim](https://store.steampowered.com/app/286160/Tabletop_Simulator/)
    * [White..hall Whitechapel? Whitesomething, Whitehall Mystery](https://www.boardgamegeek.com/boardgame/190082/whitehall-mystery)
    * [Lords of Waterdeep](https://www.boardgamegeek.com/boardgame/110327/lords-waterdeep)
    * etc.

**This week:**

## Not news!

* The Feral Corner
    * [LiS2](https://store.feralinteractive.com/en/mac-linux-games/lisbeforethestormdeluxe/)
    * [Feral Game Mode does *"something"* now](https://www.phoronix.com/scan.php?page=news_item&px=GameMode-IO-Priorities) (*ref uoou's recent experience with Nier and CPU gov's*)
    * [XCOM 2 expansion coming](https://www.reddit.com/r/linux_gaming/comments/9ks0ew/xcom_2_war_of_the_chosen_tactical_legacy_pack/)
* [More whitelisted Steamplay games](https://steamdb.info/app/891390/history/?changeid=5187653)
* [Stellaris on GOG & cross-store multiplayer beta](https://www.gog.com/game/stellaris?pp=b2a10a6c3dcadb10c8ffd734c1bab896d55cf0ec)
* [Lutris/Overwatch offers statecache now](https://www.reddit.com/r/linux_gaming/comments/9l0m2y/lutris_overwatch_installer_update_now_includes/)

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

### uoou (Drew)

* [Mastodon](https://linuxrocks.online/@uoou)
* [Ludic Linux](http://ludiclinux.com/)
* [Twitch](https://www.twitch.tv/uouniss)
> *:x :x :x :x* - Quote: Every Vim user.

## On Peertube
{% include pt.html vidid="b196e821-09d4-47e5-9017-cec126c9c99d" %}<Paste>
