This week ArchToasty and HexDSL have a lot to say about The Culling. Hex talks about things he played at the EGX show (right before the show.) 
<!--more--> 

## Host Information:

### HexDSL 

**Games:** 

* [The Culling](https://store.steampowered.com/app/437220/The_Culling/)
* [MGS V: The Phantom Pain](https://store.steampowered.com/app/287700/METAL_GEAR_SOLID_V_THE_PHANTOM_PAIN/)
* [Games at EGX](https://www.egx.net/egx/)

**This week:** 

### ArchToasty!

**Games:** 

* [Overwatch, *so much Overwatch*](https://playoverwatch.com)
* [The Culling](https://store.steampowered.com/app/437220/The_Culling/)
* [Dishonored/Dishonoured](https://store.steampowered.com/app/205100/Dishonored/)
* [Warframe](https://store.steampowered.com/app/230410/Warframe/)

**This week:** 

* [New 'installing Overwatch' video](https://youtu.be/nxUVVGCOBXs) You included my video! Thanks dude!
* [this: __GL_SHADER_DISK_CACHE_SKIP_CLEANUP](https://github.com/lutris/lutris/wiki/Performance-Tweaks#__gl_shader_disk_cache_skip_cleanup)
    * [and...](https://www.reddit.com/r/linux_gaming/comments/9gly07/nvidia_shadercache_use_this_env_variable_to_not/) new..ish method for making the cache unlimited, good for games that stutter.

## The Not News! 

* [Cross Code is out of Early access](https://steamcommunity.com/games/368340/announcements/detail/1688177728628233087)
* [Steam link now allows local multiplayer???](https://www.gamingonlinux.com/articles/the-latest-steam-client-beta-has-some-fixes-for-steam-play-steam-link-improvements-and-so-on.12597)
    *    *"Steam Link was also updated, to allow local multiplayer by streaming to multiple devices at the same time which sounds pretty sweet"*
* [Yes, Total War: Warhammer II *will* be powered by Vulkan](https://twitter.com/feralgames/status/1040206561535250438)
* [A games Linux Build was removed in favor of Proton](https://steamcommunity.com/games/474210/announcements/detail/1684800028906686550) 
* [New Nvidia drivers](https://www.gamingonlinux.com/articles/nvidia-have-releasd-the-41057-driver-as-well-as-a-3965406-vulkan-beta-driver-to-help-dxvk.12596) 
    *    *"NVIDIA have put out a new 410.57 driver and 396.54.06. nice DXVK and vulkan stuffs

## Contact us!

### ArchToasty

* [Twitter](https://twitter.com/archtoasty)
* [Twitch](https://www.twitch.tv/archtoasty)
* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Mastodon](https://linuxrocks.online/@archtoasty)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Twitter](https://twitter.com/HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube
{% include pt.html vidid="5d0d2d2f-07b2-45c5-ad73-613b9728e8b3" %}
