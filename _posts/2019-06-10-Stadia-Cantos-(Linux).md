This week on the show Hex talks about games he played, Zen kernel, kdenlive and Stadia... sooo much Stadia.
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)

* [Fell Seal](https://www.humblebundle.com/store/fell-seal-arbiters-mark?partner=hexdsl)
* [Seeds of Resilience](https://store.steampowered.com/app/877080/Seeds_of_Resilience/)
* [Steam World Quest](https://store.steampowered.com/app/804010/SteamWorld_Quest_Hand_of_Gilgamech/)
* [Octopath Traveler](https://store.steampowered.com/app/921570/OCTOPATH_TRAVELER/)

* Hex switched too the Zen kernel (peer pressure)
* Kdenlive has been a pain in my rear! 


## The Not News!

* [PROTON UPDATE! - proton-4.2-6](https://github.com/ValveSoftware/Proton/releases/tag/proton-4.2-6)
* [Stadia is a thing then](https://store.google.com/product/stadia_founders_edition)
* [Stadia cheat sheet](https://i.redd.it/zkbgiaihvv231.png)
* [Quake 2 RTX Released](https://www.nvidia.com/en-us/geforce/news/quake-ii-rtx-ray-traced-remaster-out-now-for-free/) 
 
## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="5F50ceN-5qk" %}
