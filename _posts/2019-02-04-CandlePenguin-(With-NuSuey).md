This week we talk Ion Maiden, At the Gates, Moonlighter and Stories. In the news type section we talk Metro Exodus doing an Exodus, Quake two being awesome and more... 
<!--more--> 

### HexDSL

**Games:** 

* [Ion Maiden](https://store.steampowered.com/app/562860/Ion_Maiden/)
* [At The Gates](https://store.steampowered.com/app/241000/Jon_Shafers_At_the_Gates/)
* [Yakuza 0](https://store.steampowered.com/app/638970/)

### NuSuey

**Games:** 

* [Rocksmith 2014](https://store.steampowered.com/app/221680/Rocksmith_2014_Edition__Remastered/)
* [Moonlighter](https://store.steampowered.com/app/606150/Moonlighter/)
* [My Lovely Daughter](https://store.steampowered.com/app/580170/My_Lovely_Daughter/)
* [Stories: The Path of Destinies](https://store.steampowered.com/app/439190/Stories_The_Path_of_Destinies/)

## The Not News! 

* [The Modder who fixed Dark souls has his own company now, and will make sure his games work with proton (at least he says he will)](https://www.reddit.com/r/pcgaming/comments/ak1s47/durante_opens_a_porting_studio/ef1x7k6/)
* [A better Quake 2 RTX (raytrace) demo than last week](https://youtu.be/0YYJwwSrwtg?t=63) *sorry audio listeners* 
* [Building businesses of Open Source projects](https://twitter.com/ChrisHanel/status/1090461380157665280) 
* [Metro Exodus Drama](https://www.gamingonlinux.com/articles/the-war-of-the-pc-stores-is-getting-ugly-as-metro-exodus-becomes-a-timed-epic-store-exclusive.13463)
* [Sunless skies released](https://store.steampowered.com/app/596970/SUNLESS_SKIES/)

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

### NuSuey 

* [Patreon](https://www.patreon.com/tuxdb)
* [YouTube](https://www.youtube.com/user/NuSuey)
* [Twitch](https://www.twitch.tv/nusuey)
* [tuxDB](https://tuxdb.com)

## On Youtube
{% include yt.html vidid="HcGhhNayUFQ" %}
