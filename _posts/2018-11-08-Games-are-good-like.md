This week Hex was joined by uoou (Drew) who had some interesting things to say about Cultist Simulator. Hex played (another) clicker game and there was some recent IBM/RedHat news that we strugled to have oppinions aobout. 
<!--more--> 

## Host Information:

### HexDSL 

**Games:** 

* [Project Hospital](https://store.steampowered.com/app/868360/Project_Hospital/) *Made me buy 2 Point hospital...* 
* [Hard Reset REDUX](https://store.steampowered.com/app/407810/Hard_Reset_Redux/) *SteamPlay*
* [Clicker Heroes](https://store.steampowered.com/app/363970/Clicker_Heroes/) *Steamplay - 14 hours* 

### uoou 

**Games:** 

* [Cultist Simulator](https://store.steampowered.com/app/718670)
* [Vermintide](https://store.steampowered.com/app/235540) *SteamPlay*
* [Regions of Ruin](https://store.steampowered.com/app/680360/Regions_Of_Ruin/)
* [Yooka Laylee](https://store.steampowered.com/app/360830)

## Not news!

* [The poll!](http://www.strawpoll.me/16723841) *not talking about this in show, its for audience* 
* [SPCR is now ProtonDB](https://www.protondb.com/)
* [EA's new engine has Linux & Vulkan support](https://www.phoronix.com/scan.php?page=news_item&px=EA-Halcyon-Vulkan-Linux)
* [Blue arseholes by red arseholes](https://www.phoronix.com/scan.php?page=news_item&px=IBM-Potential-Red-Hat)

## Contact me!

### HexDSL

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

### uoou

* [Mastodon](https://linuxrocks.online/@uoou)
* [Ludic Linux](http://ludiclinux.com/)
* [Twitch](https://www.twitch.tv/uouniss)

## On Peertube
{% include pt.html vidid="b3a5c943-775d-4160-8685-ba5522586161" %}
