---
podcast: false
---

**This is being posted to this website only for the first 3 episodes. after that you will need to get it from [HexDSL.co.uk](https://hexdsl.co.uk).**

I have been talking about this audio idea since November. Heres the first one. I have still not decided between HexShards or HexNuggets for the title. Feedback Welcome.

<!--more-->

<audio controls style="width:100%;">
<source src="http://hexdsl.co.uk/hexshards/HexShard_13-01-2020.mp3" type="audio/mpeg"></source>
Your browser does not support the audio element.
</audio>

[Download](http://hexdsl.co.uk/hexshards/HexShard_13-01-2020.mp3) - Right click and save it.

Thanks.

EDIT: It's called HexSHARDS based on audience early feedback. Locked it in!!! (renamed the script and everything)
