This week we got some Chris, with added Chris!!!!!! - Trigger warning, lots of Vegetable talk!
<!--more--> 

### HexDSL

* [Ys 2 (Chronicles)](https://store.steampowered.com/app/223810/Ys_I__II_Chronicles/)
* [OpenTTD (Multiplayer)](https://www.openttd.org/) *Version missmatch nightmares! - get your shit together Ubuntu!*
* [Minetest](http://www.minetest.net/)

### Chris Were

* [How are we all getting along with Steam Library Update?](https://store.steampowered.com/libraryupdate)
* [Minetest](https://www.minetest.net) [Castle footage](https://www.youtube.com/watch?v=MCUjHimFbAg)
* [Eastshade](https://store.steampowered.com/app/715560/Eastshade)
* [Mass Effect 2](https://store.steampowered.com/app/24980/Mass_Effect_2)
* [Deus Ex: Invisible War](https://store.steampowered.com/app/6920/Deus_Ex_Invisible_War)

### Not News...

* [Steam chat is actually really good!!!](https://s.team/chat/VzoDYo5y)
* [Steam may have to offer ability to sell games to other users (in frace)](https://www.gamingonlinux.com/articles/a-french-court-has-ruled-that-valve-should-allow-people-to-re-sell-their-digital-games.15054)
* [Can we just take a moment to say BLOODY GOOD work NMS!](https://www.nomanssky.com/2019/09/beyond-development-update-2/) *now 2 Linux/Proton specific fixes in a row, an unsupported platform!*

## Contact us:

### Chris Were 

* [Website](https://ChrisWere.uk)
* [Twitch](https://twitch.tv/ChrisWere)
* [Chris's new terrible podcast](https://youtube.com/ProjectChronicle)
* [Patreon](https://patreon.com/HexDSL)

### HexDSL *(Sexy Hexy)*

* [MY WEBSITE](http://hexdsl.co.uk)
* [MY Art Website (Yes, Really)](http://pixelfridge.club)
* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

## On Youtube
{% include yt.html vidid="-yP2RI8v67A" %}
