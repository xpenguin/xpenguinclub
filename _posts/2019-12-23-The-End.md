EndPenguin... VoidPenguin... OzoneRuinedTheShowPenguin... BRBPenguin... AFKPenguin... NoMorePenguin... 
<!--more--> 

### HexDSL

THIS IS THE SCHEDULES EPISODE OF XPENGUIN! - rejoice for ye' are free!

* [Hitman(TM)](https://store.steampowered.com/app/236870/HITMAN/) *I Had a masterclass With ChrisWere.* 
* [Hades](https://store.steampowered.com/app/1145360/Hades/) *Thanks Drew*

### oZone

Games you played and want to talk about!

* [Hive Time](https://cheeseness.itch.io/hive-time)
* [Odd Realm](https://store.steampowered.com/app/688060/Odd_Realm/)
* [Space Haven](https://store.steampowered.com/app/979110/Space_Haven/)
* [Fallout New Vegas](https://store.steampowered.com/app/22380/Fallout_New_Vegas/)
* [Morrowind](https://store.steampowered.com/app/22320/The_Elder_Scrolls_III_Morrowind_Game_of_the_Year_Edition/)
* [Stadia](https://stadia.google.com)

### Not The News

* [KRITA got Epic Mega Grant!](https://krita.org/en/item/krita-receives-epic-megagrant/) *They must want to be free of the Adobe Subscriptions!*
* [Want to access google with QuteBrowser? This reddit post helps!](https://old.reddit.com/r/qutebrowser/comments/ectyti/cant_login_to_your_google_account_anymore_add/) *thought I should point to this as I made a lot of QB content over the last year, I'm on Firefox now*
* [Stadia has Achievements now](https://stadia.google.com/profile/gameactivities/all) *why do people want this?* 
* [Windows should be worried?](https://www.windowscentral.com/gaming-linux-has-come-long-way)

## Contact us:

### oZone

* [Mastodon](https://linuxrocks.online/@ozoned)
* [Email](mailto:ozonedpm@pm.me)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [My Website](http://hexdsl.co.uk)
* [.XPenguin Website](http://xpenguin.club)
* [YouTube](http://youtube.com/user/hexdsl)
* [Twitch](http://twitch.tv/hexdsl)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

## On Youtube

{% include yt.html vidid="AN7HDwz-h6k" %}
