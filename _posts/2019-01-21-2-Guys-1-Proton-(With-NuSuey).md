This week Hex was joined by NuSuey for a good ol' talk about Linux stuff, fun was had by all and we spent a little bit of time talking about Stellaris, Rocksmith, Microsoft Video card walls and much more! 
<!--more--> 
## HexDSL

**Games:** 

* [Stellaris](https://store.steampowered.com/app/281990/Stellaris/)
* [Heartbound](https://store.steampowered.com/app/567380/Heartbound/)
* [Smith and Winston](https://store.steampowered.com/app/942250/Smith_and_Winston/)

UPDATE ON THE RASPBERRY PI EXPERIMENT (spoiler, its not happening... yet!)

### NuSuey! 
* [Rocksmith 2014](https://store.steampowered.com/app/221680/Rocksmith_2014_Edition__Remastered/)
* [Darkest Dungeon](https://store.steampowered.com/app/262060/Darkest_Dungeon/)
* [Neoverse (aka 'Waifu Spire')](https://store.steampowered.com/app/994220/NEOVERSE/)
* [Hello Neighbor](https://store.steampowered.com/app/521890/Hello_Neighbor/)

**This week:** 
    
* Finally back to streaming
* Random PC freezes surprising discovery how I solved them
* Double-HDD failure
* Newest MESA & Unreal Engine freeze fix for AMD

## The Not News! 

* [Glorious EggHat](https://twitter.com/GloriousEggroll/status/1085273751657291777)
* [Microsoft Graphics card wall is pretty cool](https://blogs.msdn.microsoft.com/directx/2019/01/07/wall-of-gpu-history/)
* [Steam beta update allows forced windows versions](https://www.gamingonlinux.com/articles/another-steam-client-beta-is-out-adds-the-ability-to-force-steam-play.13376)
* [ProtonTricks maintainer installs windows 10](https://www.reddit.com/r/linux_gaming/comments/agsk0f/the_creator_and_maintainer_of_protontricks_has/)
* [New 10Tons Game Coming! DYSMANTLE](https://store.steampowered.com/app/846770/DYSMANTLE/)

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

### NuSuey 

* [Patreon](https://www.patreon.com/tuxdb)
* [YouTube](https://www.youtube.com/user/NuSuey)
* [Twitch](https://www.twitch.tv/nusuey)
* [tuxDB](https://tuxdb.com)

## On Youtube
{% include yt.html vidid="1Ntg9BIzPLc" %}
