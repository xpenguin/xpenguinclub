This week turned into the Artifact show. I can't say anyone is particularly suprised at this! Hex also talks about his adventures in Crusader Kings 2 and a falure with the Longest Journey. In the news this week we talk about Sierra source code, intel GPU's and more...
<!--more--> 

### HexDSL 

**Games:** 
* [Artifact!](https://store.steampowered.com/app/583950/Artifact/)
* [Crusader Kings 2 (CK2)](https://store.steampowered.com/app/203770/Crusader_Kings_II/)
* [The Longest Journey](https://store.steampowered.com/app/6310/The_Longest_Journey/)

* Still on Browser based discord! 
* Did some Voice over stuff for the Hexys 2018! 

## The Not News! 
* [Sierra game code, now on ebay](https://arstechnica.com/gaming/2018/11/al-lowe-reveals-his-sierra-source-code-collection-then-puts-all-of-it-on-ebay/) Tip by Sudoshred
* [Intel confirm its 2020 GPU release will support Linux](https://www.phoronix.com/scan.php?page=news_item&px=Intel-dGPU-Will-Do-Linux-Gaming)
* [Valve do BUSINESS!](https://www.gamingonlinux.com/articles/valve-have-adjusted-their-revenue-share-for-bigger-titles-on-steam.13080)

## Contact us!

### HexDSL

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube
{% include pt.html vidid="53542c1c-6645-4704-96c2-c1d493ba4f50" %}
