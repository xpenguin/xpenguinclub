All Toast, No lag! this weeks fun packed episode has arch toasty talking Warframe, Hex talking Pathway. We all have a lol at google and then we bash Albion online for a bit :) 
<!--more--> 

### HexDSL

**Games:** 

* [Pathway](https://store.steampowered.com/app/546430/Pathway/)
* [Risk of Rain 2](https://store.steampowered.com/app/632360/Risk_of_Rain_2/)
* [BL1GOTY-E](https://store.steampowered.com/app/729040/Borderlands_Game_of_the_Year_Enhanced/)
* [Tropico 6](https://store.steampowered.com/app/492720/Tropico_6/)
* [Putt-Putt games](https://www.humblebundle.com/games/humongous-entertainment-bundle?partner=hexdsl) *partner link warning*
* [Albion online](https://store.steampowered.com/app/761890/Albion_Online/)

**IRC is STILL real:** Freenode #hexdsl   <-- Better than Matrix!!!! 

Looking for IRC bot to announce streams and youtube videos!

### Toasty

**Games:** 

* [Valley](https://store.steampowered.com/app/378610/Valley/)
* [Risk of Rain 2](https://store.steampowered.com/app/632360/Risk_of_Rain_2/)
* [Warframe](https://store.steampowered.com/app/230410/Warframe/)

## The Not News!

* [GZ Doom has VULKAN SUPPORT NOW!!!!](https://forum.zdoom.org/viewtopic.php?t=64188&p=1098376)
* [Matrix security issue explained](https://matrix.org/blog/2019/04/11/security-incident/)
* [Albion online is free](https://albiononline.com/en/news) ... but *still* shit 
* [Godot got that foxy funding](https://www.gamingonlinux.com/articles/godot-engine-awarded-50k-usd-from-mozilla-more-exciting-features-planned-for-godot-engine-32.13936) *thanks LinuxPaulM* 
* [A GAME JAM!!!](https://itch.io/jam/linux-game-jam-2019) *thanks Cheeseness* 
* [Google forgot to renew its apt sigs](https://reddit.com/r/linux/comments/bcbmoi/google_forgot_to_renew_their_apt_repository/)
  
## Contact us!

### ArchToasty 

* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Twitch](https://www.twitch.tv/archtoasty)
* [Mastodon](https://linuxrocks.online/@archtoasty)
* [Email](archtoasty@protonmail.com)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="6Dw1RzEVUtw" %}
