Hex and Hamish talk about Outward, Monster hunting, Duskers and Magic's new battlepass thing. There is also some Planet Explorers grumblings, Steam Controller thoughts and more.... 
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)

* [Out ward](https://store.steampowered.com/app/794260/Outward/)
* [Monster Hunter World](https://store.steampowered.com/app/582010/MONSTER_HUNTER_WORLD/)
* [60 Parsecs](https://store.steampowered.com/app/646270/60_Parsecs/) *like that other game i cant remember but less sad*

Hex is going to use his steam controller for a week

### HamishTPB

**Games:** 

* [Duskers](https://store.steampowered.com/app/254320)
* [Magic:The Gathering: Arena](https://magic.wizards.com/en/mtgarena) *Some big updates and new Core Set 2020*
* [Itch.io](https://itch.io) *I did a stream*

### Not News... 

* [GOL is 10 years old!](https://www.gamingonlinux.com/articles/10-years-ago-gamingonlinux-was-created-what-a-ride-its-been.14505)
* [Planet explorers FREE because of lost code?!](https://store.steampowered.com/app/237870/Planet_Explorers/)
* [Steam Controller 2?](https://www.pcgamesn.com/steam-controller-2) *honestly, thought steam controllers were dead!* 
* [DOTA Underlords updates](https://www.gamingonlinux.com/articles/dota-underlords-now-has-scoreboards-and-more-improvements-plus-a-proto-battle-pass-next-week.14513)
 
## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)
  
## On Youtube
{% include yt.html vidid="dYsnrVuQsfU" %}
