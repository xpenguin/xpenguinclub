This week Hex and Drew (Friendo, Uoou, whatever hes called this week) talk Disco Elysium, Rage 2, Stadia (again) Kerbal Space Program(me) and much much more. 
<!--more--> 

[A onetab link](https://www.one-tab.com/page/_Ngi6pXhRy2voyZxZPa9MQ)

### HexDSL

* [Destiny 2 (Stadia)](https://store.steampowered.com/app/1085660/Destiny_2/) *STILL DOESNT WORK ON STEAM*
* [Rage 2](https://store.steampowered.com/app/548570/RAGE_2/) *Steam Sale price was great!*
* [Disco Elysium](https://store.steampowered.com/app/632470/Disco_Elysium/) *good game, not for hex*

### Friendo

* [Disco Elysium](https://store.steampowered.com/app/632470/Disco_Elysium/)
* [Kerbal Space Program(me)](https://store.steampowered.com/app/220200/Kerbal_Space_Program/)

### Not The News

* [Lurtis get paid](https://www.patreon.com/posts/lutris-is-epic-31951429?utm_medium=post_notification_email&utm_source=post_link&utm_campaign=patron_engagement)
* [Stadia](https://stadia.com)
    * [Some dude got a Stadia Update](https://www.eurogamer.net/articles/2019-11-27-google-im-not-sure-thats-how-stadia-is-supposed-to-work)
    * [Spitlings!](https://www.eurogamer.net/articles/2019-11-26-co-op-arcade-game-spitlings-becomes-google-stadias-second-timed-exclusive#comments)
    * [Stadia controller added to SDL2](https://www.phoronix.com/scan.php?page=news_item&px=SDL2-Stadia-Controller)
* [Boiling Steam, "We dont Game on the Same Distros No More"](https://boilingsteam.com/we-dont-game-on-the-same-distros-no-more/)
* [New GE-Proton Build and its a good one](https://github.com/GloriousEggroll/proton-ge-custom/releases/tag/4.20-GE-1)
* [New Lutris and its a good one](https://github.com/lutris/lutris/releases/tag/v0.5.4)
* [Steam Controller is dead now](https://www.theverge.com/good-deals/2019/11/26/20984123/valve-steam-controller-discontinued-sale-price)

## Contact us:

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [My Website](http://hexdsl.co.uk)
* [.XPenguin Website](http://xpenguin.club)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

### Friendo

* [World Wide Web](https://friendo.monster/)
* [Masdtodon](https://linuxrocks.online/@uoou)

## On Youtube

{% include yt.html vidid="Xm8xoTpuShg" %}
