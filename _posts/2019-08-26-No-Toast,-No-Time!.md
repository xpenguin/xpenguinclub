This week hes was unexpectedly solo. Its a short but focused show where i talk Warfork, Orange Juice, Visal Novels and More. 
<!--more--> 

### HexDSL

* [100% Organge Juice](https://store.steampowered.com/app/282800/)
* [Warfork](https://store.steampowered.com/app/671610/Warfork/)
* [New HEXDSL Website is a thing](http://hexdsl.co.uk)
* [MY Art Website (Yes, Really)](https://pixefridge.club)

### Not News... 

* [Looks like vkd3d will be in proton soon](https://github.com/ValveSoftware/Proton/commits/proton_4.11-2-vkd3d)
	* [Pheronix confirmed](https://www.phoronix.com/scan.php?page=news_item&px=VKD3D-Proton-Building) 
	* VKD3D = Direct3D 12  - DXVK = Direct3D 11 / Direct3D 10 - D9VK = Direct3D 9
* [War Thunder Dev things we should be greatful!](https://www.reddit.com/r/linux_gaming/comments/cukgio/be_thankful_there_is_linux_version_at_all_war/)
* [Fuck this headline!](https://www.omgubuntu.co.uk/2019/07/drawing-microsoft-paint-alternative-for-linux)

## Contact us!

### HexDSL *(Sexy Hexy)*

* [MY WEBSITE](http://hexdsl.co.uk)
* [MY Art Website (Yes, Really)](http://pixelfridge.club)
* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

## On Youtube
{% include yt.html vidid="EE0RVjDj-Q4" %}
