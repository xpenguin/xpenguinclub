This week Hex is joined by Drew to talk about Jupiter Hell, 7 Days to Die and CK2... 2.
<!--more--> 

[A onetab link](https://www.one-tab.com/page/Ln_USTKUS1CPIW2NTkvKlQ)

### HexDSL: Games

* [Jupiter Hell](https://store.steampowered.com/app/811320/Jupiter_Hell/)
* [Tales of Maj'Eyal](https://store.steampowered.com/app/259680/Tales_of_MajEyal/)
* [Dominus Galaxia](https://dominusgalaxia.com/)
* [Pine](https://store.steampowered.com/app/1042780/Pine/)

### UOOU

* [7 Days to Die](https://store.steampowered.com/app/251570/7_Days_to_Die/)

### Not The News

* [Steam Remote Play Together broken on Linux](https://github.com/ValveSoftware/steam-for-linux/issues/6605) *good job Valve - also Broadcasting STILL not working in Linix*
    * [And on steam](https://steamcommunity.com/groups/homestream/discussions/1/3159763879062781724/)
* [CK2 has a new features that dont work in linux](https://forum.paradoxplaza.com/forum/index.php?threads/ck-ii-ruler-challenges-not-showing-up-under-linux.1262855/#post-25929902) *more Proof that 'Support' is pointless an temporary!*
    * [CK2-2 Linux Confirmed](https://www.gamingonlinux.com/articles/we-have-it-confirmed-that-crusader-kings-iii-will-be-releasing-for-linux.15270)
* [FlightlessMango - DXVK benchmarking thingy](https://github.com/flightlessmango/dxvk) *did not know this exsited until reddit told me*
* [Solus Creator makes games now.... okay](https://lispysnake.com/blog/2019/10/20/enter-the-miniature-dragon/)
* [BoxTron Currently AWESOME!](https://github.com/dreamer/boxtron/releases/tag/v0.5.3) *supports DOSBox-Staging now and its GREAT!*
* [Disney+ Not working on Linux](https://www.omgubuntu.co.uk/2019/10/happily-never-after-heres-why-disney-doesnt-support-linux?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+d0od+%28OMG%21+Ubuntu%21%29)
* [Unix is 50 now](https://www.bell-labs.com/unix50/)

## Contact us:

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [My Website](http://hexdsl.co.uk)
* [My Art Blog (Yes, Really)](http://pixelfridge.club)
* [.XPenguin Website](http://xpenguin.club)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

### UOOU

* [Mastodon](https://linuxrocks.online/@uoou)

  
## On Youtube
{% include yt.html vidid="m-eun4ooNbg" %}
