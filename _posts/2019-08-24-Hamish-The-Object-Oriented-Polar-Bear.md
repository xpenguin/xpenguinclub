Hex and Hamish Talk Flight sims, HOTAS, space mercs and more!
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)

* [Aseprite](https://store.steampowered.com/app/431730/Aseprite/) *update from last week* 
* [Space Mercs](https://store.steampowered.com/app/1088600/)
* [Ion ~~Maiden~~ Fury](https://store.steampowered.com/app/562860/Ion_Fury/)
* [X4: Foundation](https://store.steampowered.com/app/392160/)
* [HexDSL Website, now an actual thing.](http://www.hexdsl.co.uk)

### Hamish 

**Games:**

* [Himno](https://store.steampowered.com/app/931690/Himno/)
* [X-Plane 11](https://store.steampowered.com/app/269950/XPlane_11/)
* [Space Mercs](https://store.steampowered.com/app/1088600/)
* [Quadrant](https://store.steampowered.com/app/365320/Quadrant/)

**Other:**

* Programming ("back in the saddle")
* Website the pitfalls of relying on "engines".
* Friday Fright Night back on my stream

### TOPICS

* [Rust do the right thing](https://rust.facepunch.com/blog/updated-linux-plans/) *actully a really good move and is really the BEST damage control they CAN do. EVERYONE should refund this to prove that there are a substantial amount of Linux players. just to prove a point after the last post!* 

## Conact

### Hamish The PolarBear

* [Website](https://thepolarbear.co.uk)
* [Twitch](https://twitch.tv/hamishtpb)
* [Mastodon](https://linuxrocks.online/@HamishTPB)
* [Email](hamish@thepolarbear.co.uk)
* [YouTube](https://www.youtube.com/channel/UCp1mWfjYbMcmNowBmvTUCag)
* [PeerTube](https://peertube.linuxrocks.online/accounts/hamishtpb/videos)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [My Website](http://www.hexdsl.co.uk)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)
  
## On Youtube
{% include yt.html vidid="y96Tn0pHG4Y" %}
