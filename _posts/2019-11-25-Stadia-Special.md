The .XPenguin Stadia Special! - STREAMING IS THE FUTURE! FACT!
<!--more--> 

[A onetab link](https://www.one-tab.com/page/INUlFHOSSFiFVJW1WWceFg)

### STADIA

* [Stadia](https://stadia.com)
    * Destiny 2
    * Final Fantasy XV
    * Tomb Raider
* [Digital Foundry video](https://youtu.be/9uwunZ2a4gc)
* [Price of controllers IMO too high?](https://store.google.com/gb/config/stadia_premiere_edition)
* [Gaming On Linux Article](https://www.gamingonlinux.com/articles/some-early-first-impressions-of-google-stadia-played-on-linux.15466)
> 💬 Amazon prices: PS4 controller = £44.99 | Xbox One = £44.35 | Switch Controller = £53.99 | Switch Joycons = £62.99 - I assume that Stadia controller will get cheaper when they hit General Stores. 

*Cyberus:* 

* While I'm hesitant to jump into the politics of games journalism, I feel its worth mentioning that it has been clearly skewed towards Stadia I feel unreasonably so!
* User Handles seeming to be something they see as a commodity?
* Are we as linux users too forgiving?
* Can we Trust Google?

> FAST FACTS! You need for an Android device to set up!

### Not The News

* [New HL VR game is WINDOWS ONLY?](https://store.steampowered.com/app/546560/)
* [GLIMPSE!](https://www.omgubuntu.co.uk/2019/11/first-glimpse-image-editor-release?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+d0od+%28OMG%21+Ubuntu%21%29)
* [This article about binding mouse keys to keyboard](https://www.linuxuprising.com/2019/11/how-to-bind-mouse-buttons-to-keyboard.html)

## Contact us:

### CyberusUK

Website in the making, keep you all informed!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [My Website](http://hexdsl.co.uk)
* [.XPenguin Website](http://xpenguin.club)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

## On Youtube
{% include yt.html vidid="Ed9JM8VunoY" %}
