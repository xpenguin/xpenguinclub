This weeks I talk in with Corben about his adventures in Everspace and how he ended up streaming on the Rockfish Games Steam page. we also make some comments on that whole Overwatch Banning wave that got some penguins in trouble over the past few days...
<!--more--> 

## Host Information:

### HexDSL 

**Games:** 

* [Space Food Truck](https://store.steampowered.com/app/397390/Space_Food_Truck/) - Seems to be some sync issues with multiplatform play. 

**This week:** 

### Corben

**Games:** 
 
 the Everspace Journey.... 
* [Everspace](http://store.steampowered.com/app/396750/EVERSPACE/)
    * Streaming Everspace (Story about how I was allowed to stream Everspace on the official channels)
    * Everspace on [Kickstarter](https://www.kickstarter.com/projects/rockfishgames/everspace), Linux as a stretch goal
    * Issues with Unreal Engine
    * Met CEO Michael Schade on GamesCom 2017 in hotel lobby
    * 1 Year after Windows and Mac Release came the Linux Release
    * 1st **cookie package** as a "Thank You" inspired by [Apyr Media and Feral Interactive receiving packages](https://www.gamingonlinux.com/articles/linux-community-has-been-sending-their-love-to-feral-interactive-aspyr-media.8416)
    * [Linux Steam Thread](https://steamcommunity.com/app/396750/discussions/0/333656722966793785/?ctp=25#c1694922526917807924), Cheeseness pointed out an answer about VR (**cookie package** arrived!)
    * Patch 1.3.0 released: 2nd **cookie package**: [Twitter](https://twitter.com/Rockfishgames/status/1018796430662819841)
    * Everspace Release Party: [Photos on FB](https://www.facebook.com/media/set/?set=a.1691275977592270.1073741831.836632023056674&type=1&l=75d6bde6ee)
    * Won a ticket to watch the premiere of [space.games.film](http://space.games.film)
    * GamesCom 2018, met Michael Schade, Christian Lohr, Will ([shiuzan](https://twitter.com/William_Turrall)), Erik ([giraffasaur](https://twitter.com/Giraffasaur))
    * Patch 1.3.2 released: 3rd cookie package
* [Beat Saber](https://store.steampowered.com/app/620980/Beat_Saber/)
    * Streaming mixed reality (inspired by [Mohero](https://www.twitch.tv/moheromogellor))

**This week:** 

* Trying to get rid of wine prefixes thanks to proton (working: No Man's Sky, not working: Aliens vs Predator (2010), not working: Aliens Colonial Marines)
* Surface Pro 3
* Upgrading Ubuntu 16.04 to 18.04
* Laravel, VueJS, PHP Unit Test

## The Not News! 

* [MoveMaster](http://movemaster.biz)
* [Life is Strange: Before the Storm is out!](https://store.steampowered.com/app/554620/Life_is_Strange_Before_the_Storm/)
* [DXVK getting people banned in overwatch?](https://www.reddit.com/r/linux_gaming/comments/9fkuq9/overwatch_avoid_async_option_for_dxvk_banned_for/)
    * [Phoronix article](https://www.phoronix.com/scan.php?page=news_item&px=Blizzard-Banning-DXVK-Wine)
* There are two GOL interviews this week that are well worth a read.
    * [FNA interview (GOL)](https://www.gamingonlinux.com/articles/game-porter-ethan-lee-gives-his-thoughts-on-valves-steam-play-and-proton.12542)
    * [DXVK interview (GOL)](https://www.gamingonlinux.com/articles/an-interview-with-the-developer-of-dxvk-part-of-what-makes-valves-steam-play-tick.12537)
* [Cheesetalks article](http://cheesetalks.net/proton-linux-gaming-history.php)
* [Hand of Fate 2 Modding Contest](https://steamcommunity.com/games/456670/announcements/detail/1707317393254056089)

## Contact us!

### Corben

* [Twitter](https://twitter.com/corben78)
* [Twitch](https://twitch.tv/corben78)
* [Smashcast TV](https://smashcast.tv/corben)
* [YouTube](https://youtube.com/user/c0rben)
* [Mastodon](https://mastodon.social/@Corben78)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Twitter](https://twitter.com/HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube
{% include pt.html vidid="96dacf53-7e35-44a2-9138-83168dfa08d4" %}
