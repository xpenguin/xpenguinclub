I think this can at this point be called 'the borderlands Episode' and thats okay. I also talked about IRC and Tropico 6
<!--more--> 

### HexDSL

**Games:** 

* [Risk of Rain 2](https://store.steampowered.com/app/632360/Risk_of_Rain_2/)
* [Borderlands 1 ENHANCED](https://store.steampowered.com/app/729040/)
* [Tropico 6](https://store.steampowered.com/app/492720/Tropico_6/)
* [Fate Hunters](https://store.steampowered.com/app/920680/Fate_Hunters/)

**IRC is real:** Freenode #hexdsl

## The Not News!

* [Borderlands 3 is canceled](https://www.youtube.com/watch?v=JiEu23G4onM)
* [Borderlands 2 UHD may happen](https://www.reddit.com/r/linux_gaming/comments/b92l6k/aspyr_will_investigate_integrating_the/)
    *    [ Will Happen](https://www.gamingonlinux.com/articles/aspyr-media-confirm-the-free-ultra-hd-dlc-for-borderlands-2-and-the-pre-sequel-is-coming-to-linux.13896)
* [Borderlands 1 ENHANCED](https://store.steampowered.com/app/729040/)
    * YES the rename trick works on Borderlands 2 Windows version via proton. 
* [Proton Updated numbers and things](https://www.reddit.com/r/linux_gaming/comments/b9tqfn/protondb_now_reports_60_of_all_games_tested_as/)

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="Nz5LIaiXK0o" %}
