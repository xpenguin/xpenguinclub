This week ArchToasty and I talk about games and stuff, including a mutual hate of Epic Store, Love of Factorio and a lot of off topic banter. 
<!--more--> 

### HexDSL

**Games:** 

* [Vulcanoids](https://store.steampowered.com/app/951440/Volcanoids/)
* [Tower Unite](https://store.steampowered.com/app/394690/Tower_Unite/) *proton*
* [Factorio](https://store.steampowered.com/app/427520/Factorio/)

* [The BEST bundle on steam!](https://store.steampowered.com/bundle/9849/BUNDLE/) *Yes, i did buy it!* 

* my pokemon was empty!!!!
* Hex's Stupid Keyboard update! *He LOVES it now!*

### ArchToasty

**Games:** 

* [Devil May Cry 5](https://store.steampowered.com/app/601150/Devil_May_Cry_5/)


## The Not News!

* [Artifact, now properly, for reals dead?](https://www.artibuff.com/blog/2019-03-08-garfield-is-no-longer-at-valve) *Thanks NuSuey*
* [Battle eye on Linux, abandon all hope?](https://www.gamingonlinux.com/articles/seems-like-theres-no-hope-for-battleye-support-within-steam-play.13747)
* [Steam from ANYWHERE!](https://steamcommunity.com/app/353380/discussions/0/3362406825533023360/)
* [GloriousEggroll Proton](https://drive.google.com/open?id=1CPUgU8l7uB-YgHbXXgUOyK6yfQkspL5v)
* [That Epic Tweet](https://www.reddit.com/r/linux_gaming/comments/b0zph4/sergey_galyonkin_says_that_epic_is_looking_for/)
* [Feral Game Mode shite. its still a thing](https://www.gamingonlinux.com/articles/feral-interactive-have-put-out-a-big-update-to-their-gamemode-linux-gaming-performance-tool.13767)
* [Gnome 3.32 update coming](https://www.gnome.org/news/2019/03/gnome-3-32-released/) *NuSuey made me talk about this*

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

### ArchToasty 

* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Twitch](https://www.twitch.tv/archtoasty)
* [Mastodon](https://linuxrocks.online/@archtoasty)
* [Email](archtoasty@protonmail.com)

## On Youtube
{% include yt.html vidid="aaqDZknZAxo" %}
