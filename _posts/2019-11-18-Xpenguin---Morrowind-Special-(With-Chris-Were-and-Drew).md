Chris Were and Drew join Hex for a Morrowind Multiplayer SPECIAL. Mostly we talk about why Morrowind is best game and why Skyrim bad game. Also, we have no feelings on Oblivion, in case you wondered. 
<!--more-->

### HexDSL & Chris & Drew

* [OpenMW/TES3MP - Multiplayer](https://openmw.org/en/)
    * [TES3MP](https://tes3mp.com/)

Things it has: 
* Improved View distances.
* High resolution Graphics.
* Multiplayer, with very few downsides.
* Really great performance. 
* Made a script that opens it on steam so I can track hours. 

Things it should fix:
* Terrible security 
> 💬 Drew: ^ that's my fault!
* Janky multiplayer options! (we all get Quest if one person does)
* Occasional de-syncing

Things we did:
* Balmora is basically fucked.
* Police chased us out of Balmora and Drew had was the most wanted man alive. 
* I jumped into the sky. and died. Thanks for that one Chris and Drew!
* I Spent 30k trying to get a woman to punch me! (then)
* Started a fight witha bunch of mages. (got killed)
* Started a fight with the whole Corner Club. (got killed)
* ~~Started a fight with~~ (<-Drew edited that) Murdered some miners. (got killed)
> 💬 Hex: ^ They Had it coming! 
* Started a fight with man in a shop. (got killed)
* Also, I get beaten up every time I sleep and it is not cool
> 💬 Drew: ^ But it kinda is cool because we make a lot of money

### Not The News

MORROWIND!!! 

## Contact us:

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [My Website](http://hexdsl.co.uk)
* [.XPenguin Website](http://xpenguin.club)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)

### Chris Were

* [Mastodon](https://linuxrocks.online/@ChrisWere)
* [World Wide Web](https://ChrisWere.uk)

### Drewiepops

* [Mastodon](https://linuxrocks.online/@uoou)
* [World Wide Web](https://friendo.monster/)

## On Youtube
{% include yt.html vidid="cfo6B8Kx0-c" %}
