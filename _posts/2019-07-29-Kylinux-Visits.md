This week we have a KyLinux (cast) so I guess its a KYPenguin. 
<!--more--> 

### HexDSL

* [Ys Origin](https://store.steampowered.com/app/207350/) *Thanks KyLinux, also mention Ys 1&2 - thanks Sk4zZi0uS*
* [CS:GO DANGER ZONE](https://store.steampowered.com/app/730/) *first time in the danger zone - it was shit* 

### KyLinux

GAMES AND STUFF!!! 

* [Ys Origin](https://store.steampowered.com/app/207350/)
* [Cave Story](https://store.steampowered.com/app/200900/Cave_Story/) *This game deserves so much love*
* [Monster Hunter World](https://store.steampowered.com/app/582010/MONSTER_HUNTER_WORLD/)
* [Outward](https://store.steampowered.com/app/794260/Outward/) *We can keep it brief, but I really want to discuss the lie that is this game*
* [Guild Wars 2](https://www.guildwars2.com/en/)

### Not News... 

* [Dicey Dungeons release date!](https://store.steampowered.com/app/861540/Dicey_Dungeons/)
* [Oxygen not included has release date!](https://steamcommunity.com/games/457140/announcements/detail/1602637235914852213) *leaving Early access*
* [Developer who works in linux doesn't want to release Linux version](https://www.reddit.com/r/linux_gaming/comments/chslal/i_moved_my_entire_game_development_platform_to/)

## Contact us!

### KyLinux

* [YouTube](http://youtube.com/kylinuxcast)
* [Twitch](http://twitch.tv/kylinuxcast)
* [Mastodon](https://linuxrocks.online/@KylinuxCast)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)
  
## On Youtube
{% include yt.html vidid="drSLlV6gdUM" %}
