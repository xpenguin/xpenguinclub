This week Hex is joined by Cheese! there is Hand of Fate 2 talk, some discussion about The Away Team's upcoming changes and Hex squee's like a fanboy about Nier, this and more in this weeks fun packed episode... okay, fun packed is a stretch but you get the idea... 
<!--more--> 

## Host Information:

### HexDSL 

**Games:** 

* [NieR: Automata](https://store.steampowered.com/app/524220/NieRAutomata/) *SteamPlay*
* [BATTLETECH](https://store.steampowered.com/app/637090/BATTLETECH/) *Beta*
* [The Away Team: Lost Exodus](http://awayteam.space/)

### Cheese

**Games:** 

* [Hand of Fate 2](https://store.steampowered.com/app/456670/Hand_of_Fate_2/)

**This week:** 

* [FutureFest](https://twitter.com/ValiantCheese/status/1052372043293839360)
* [Hand of Fate 2 Briarfolk Modding Contest](https://steamcommunity.com/games/456670/announcements/detail/1707317393290552085)
* [Hand of Fate 2 Community Modding Wiki](https://hof2modwiki.cheesetalks.net/)
* [The Away Team: Lost Exodus](http://awayteam.space/) (find some better URL - hopefully we'll have more blag pasts out by then)
* [New Away Team trailer](https://www.youtube.com/watch?v=AZqXnaq4PAo)
* [Redesigning the Argo](https://underflowstudios.com/the-away-team-lost-exodus-redesigning-the-argo/)

## Not news!

* [Rimworld left Early Access](https://store.steampowered.com/app/294100/RimWorld/)
* [Discord now sell games, the store works on Linux but the games don't.](https://blog.discordapp.com/discord-store-global-beta-is-live-38bfd044d648)
* [Ethan Lee now works on Proton (officially)](https://mobile.twitter.com/flibitijibibo/status/1051967505160388614)
* [Eye Anticheat stated to a user that they cant support wine but are looking into Steamplay?!?!?](https://cdn.discordapp.com/attachments/406546687268487171/502410626803564544/Screenshot_2018-10-18_11-19-31.png)
* [Elite Dangerous appears to be working in wine now!](https://forums.frontier.co.uk/showthread.php/366894-How-to-install-ED-on-Linux-using-Wine-EXPERIMENTAL-NOT-OFFICIALLY-SUPPORTED?p=7082698&viewfull=1#post7082698) - Thanks Nusuey for tip. 
* [Eastshade](http://www.eastshade.com/)
* [Run Windows versions of Linux games](https://github.com/Holston5/Native2Proton)

## Contact us!

### Cheese

* [Twitter](http://twitter.com/valiantcheese)
* [Mastodon](https://mastodon.social/@Cheeseness)
* [Pamtram](https://www.patreon.com/Cheeseness/)
* [Twitch](http://twitch.tv/valiantcheese/)

### HexDSL

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Peertube
{% include pt.html vidid="16fb7ff6-3567-4648-92f5-7760d776e141"%}
