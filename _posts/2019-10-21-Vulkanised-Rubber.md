This week Hex talks Kingsway, Lord of the rings online, Ourward and mulls over CK2-2, Stadia and its immanant arrival as well as gnu-fsf blogs post and much more... 
<!--more--> 

[A onetab link](https://www.one-tab.com/page/gTIUqq-iTcGRw4JVz7WVrA)

### HexDSL

**Games:** (there be affiliate links below!)

* [Kingsway](https://store.steampowered.com/app/588950/Kingsway/) *Still Good.*
* [Outward](https://store.steampowered.com/app/794260/Outward/)
* [Shadow of war](https://store.steampowered.com/app/241930/Middleearth_Shadow_of_Mordor/)
* [Lord of the rings online... yes really](https://store.steampowered.com/app/212500/The_Lord_of_the_Rings_Online/)

### Not News... 

* [ScummVM now supports BLADERUNNER!](https://www.scummvm.org/news/20191013/)
* [Shadow of Vulkan!](https://www.gamingonlinux.com/articles/the-linux-port-of-shadow-of-mordor-from-feral-interactive-has-gained-a-vulkan-beta-a-massive-difference.15226)
* [CK2 2 is coming](https://store.steampowered.com/app/1158310/Crusader_Kings_III/)
* [Stadia is coming!](https://www.blog.google/products/stadia/stadia-arrives-november-19/)
* [System 76 BEGIN SHIPPING core boot](https://www.forbes.com/sites/jasonevangelho/2019/10/10/system76-will-begin-shipping-2-linux-laptops-with-coreboot-based-open-source-firmware/#736c9fbc4e64) *forbes writeup is actually pretty good)*
* [GNU and FSF still playing nice?](https://www.fsf.org/news/fsf-and-gnu)

## Contact me.

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [My Website](http://hexdsl.co.uk)
* [My Art Blog (Yes, Really)](http://pixelfridge.club)
* [.XPenguin Website](http://xpenguin.club)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)
  
## On Youtube
{% include yt.html vidid="ehyojPxcRUE" %}
