This week Hex praises Puppy games, talks about Tower Unite and has a ramble about keyboards (again). Also there's news and stuff

<!--more--> 

### HexDSL

**Games:** 
* [Tower Unite](https://store.steampowered.com/app/394690/Tower_Unite/)
* [Nimbatus](https://store.steampowered.com/app/383840/Nimbatus__The_Space_Drone_Constructor/)
* [Mutant Year Zero](https://store.steampowered.com/app/760060/Mutant_Year_Zero_Road_to_Eden/)
* [Z](https://store.steampowered.com/app/275530/)

* [Journey quest](https://store.steampowered.com/app/433270/JourneyQuest_Season_One/) *general thoughts on steam video*

## The Not News! 

* [PuppyGames make Linux gaming free gaming!](https://twitter.com/puppygames/status/1103070118425382914)
* [Steamplay options now in Steam BPM](https://steamcommunity.com/groups/SteamClientBeta/announcements/detail/1798531141142819058)
* [Dicey Dungeons updated!](https://www.gamingonlinux.com/articles/dicey-dungeons-the-unique-in-development-roguelike-from-terry-cavanagh-has-a-major-update.13702) 
* [Rust (game) now on Vulkan](https://rust.facepunch.com/blog/oilrig-update) *Still no Linux support*
* [WRATH - QUAKE 1 ENGINE, 3DReams!](https://twitter.com/3DRealms/status/1103627856201113601)*insta-buy!*

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="KIPSaFJGPZc" %}
