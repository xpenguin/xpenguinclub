Better than Game of Thrones! (I am told) - This week Cheese join Hex to talk BEES, and BEE GAMES and BEE RELATED THINGS. There are a lot of Bees in this weeks show! 
<!--more--> 
## HexDSL

**Games:** 

* [Pathalogic 2](https://www.humblebundle.com/store/pathologic-2?partner=hexdsl)
* [Swords of Ditto](https://www.humblebundle.com/store/the-swords-of-ditto?partner=hexdsl)
* [Descenders](https://www.humblebundle.com/store/descenders?partner=hexdsl)
* [Daikatana](https://store.steampowered.com/app/242980/Daikatana/)

## Cheese

**Playing Games:**

* [Assault Android Cactus](https://steamcommunity.com/games/250110/announcements/detail/1643161387076037278)

**Making Games:**

* [Hive Time](http://hivetime.twolofbees.com)
    * [Progress thread](https://twitter.com/ValiantCheese/status/1131177426673733633)
* [Honeycomb CRUNCH](http://hivetime.twolofbees.com)
    * [Progress thread](https://twitter.com/ValiantCheese/status/1131647338630434816)
    * Cheese is going to have a small, considered tirade about respecting people who make stuff, which may or may not go along the lines of "Fuck you dude with your unsolicited creative input into a game you didn't make, fuck you for assuming that the game is something bigger than it's clearly documented to be, fuck you for accusing me of abandoning the shit I work hard to make just because its final state isn't what you want"

## The Not News! 

* [Antergos!, again](https://forum.antergos.com/topic/11780/endeavour-antergos-community-s-next-stage)
* [someone took the time to write nice things about systemd](https://www.reddit.com/r/linux/comments/bsqar4/you_probably_hate_systemd_because_you_think_its/)
* [Johnathan blow would switch to Linux if he could be productive](https://clips.twitch.tv/GeniusSavageAniseKeyboardCat)

(note - kept topics to a minimum today as )

## Contact us!

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

### Cheese ###
* [Patreon](https://www.patreon.com/Cheeseness)
* [LiberaPay](https://liberapay.com/Cheeseness)
* [Cheese Talks](http://cheesetalks.net/)
* [Itch](http://cheeseness.itch.io/)
* [Twitter](https://twitter.com/valiantcheese)
* [Mastodon](https://mastodon.social/@Cheeseness)
* [Twitch](http://twitch.tv/valiantcheese/)

## On Youtube
{% include yt.html vidid="m6ovReFYCg0" %}
