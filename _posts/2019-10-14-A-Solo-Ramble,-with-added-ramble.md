A short Rambling show filled with nonsense and Kingsway love. 
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)

* [Kingsway](https://store.steampowered.com/app/588950/Kingsway/) *sooo... Good.*
* [Star Wars: The Old Republic](https://lutris.net/games/star-wars-the-old-republic/) *Sooo... bored.*
* [Littlewood](https://store.steampowered.com/app/894940/Littlewood/) *13 hours in, having a blast but hitting walls*
* [EDF 4.1](https://store.steampowered.com/app/410320/EARTH_DEFENSE_FORCE_41_The_Shadow_of_New_Despair/)

### Not News... 

* [Stream Remote Play Together?](https://arstechnica.com/gaming/2019/10/steams-next-big-feature-will-make-any-local-multiplayer-game-work-online/) *the cat quest 2 solution?* 

## Contact me.

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [My Website](http://hexdsl.co.uk)
* [My Art Blog (Yes, Really)](http://pixelfridge.club)
* [.XPenguin Website](http://xpenguin.club)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@posteo.net)
  
## On Youtube
{% include yt.html vidid="Tstdlic6gkE" %}
