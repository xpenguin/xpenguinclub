Hex and Toasty talk Ubuntu, Steam and much more on two hours(ish) of quality banter. 
<!--more--> 

### HexDSL

**Games:** (there be affiliate links below!)

 * [Fallout 3 - DJAZZ FIXED](https://gist.github.com/daniel-j/e89004d6d5548d648701883e736ce213)
 * [DOTA UNDERLORDS](https://store.steampowered.com/app/1046930/)
 * [Rusty Lake Hotel](https://store.steampowered.com/app/435120/) *played and finished on stream* 
 * [Lovely Planet 2 April Skies](https://store.steampowered.com/app/1019590/Lovely_Planet_2_April_Skies/)
 * [Cryptark](https://store.steampowered.com/app/344740/CRYPTARK/)

### Toasty

**Games:** 

* [Undertale](https://store.steampowered.com/app/391540/Undertale/)
* [DOTA UNDERLORDS (Twinsies!)](https://store.steampowered.com/app/1046930/)
* [Oddworld New n Tasty](https://store.steampowered.com/app/314660/Oddworld_New_n_Tasty/)

### THE STEAM SECTION 

* [Valve don't support Ubuntu any more!!!](https://twitter.com/Plagman2/status/1142262103106973698)
* [Popey cant get his games working without 32bit](https://discourse.ubuntu.com/t/results-of-testing-games-on-64-bit-only-eoan-19-10/11353)
* [Steam connection issues](https://github.com/ValveSoftware/steam-for-linux/issues/6326)
* [WINE dropping Ubuntu Support.... hummm](https://www.winehq.org/pipermail/wine-devel/2019-June/147869.html)
* [Mr Eggroll made awesome proton thing again!](https://github.com/GloriousEggroll/proton-ge-custom/releases/tag/4.10-GE-2) *gosh, its good* 
 
## Contact us!

### ArchToasty 

* [YouTube](https://www.youtube.com/channel/UCeqOy4HEegSyMQxwdEtcgnQ)
* [Twitch](https://www.twitch.tv/archtoasty)
* [Mastodon](https://linuxrocks.online/@archtoasty)
* [Email](archtoasty@protonmail.com)

### HexDSL *(Sexy Hexy)*

* [Patreon](https://www.patreon.com/hexdsl)
* [YouTube](http://youtube.com/user/hexdsl)
* [PeerTube](https://share.tube/accounts/hexdsl/videos)
* [Twitch](http://twitch.tv/hexdsl)
* [.XPenguin Website](http://xpenguin.club)
* [Mastodon](https://linuxrocks.online/@HexDSL)
* [Email](mailto:hexdsl@protonmail.com)

## On Youtube
{% include yt.html vidid="7IXWRJKbyag" %}
